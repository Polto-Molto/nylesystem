#include "dialogsetlotsetup.h"
#include "ui_dialogsetlotsetup.h"
#include <QButtonGroup>
#include <QDebug>

DialogSetLotSetup::DialogSetLotSetup(dataManager *d,int id,modbus_action* m,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogSetLotSetup),
    _datamgr(d),
    kiln_id(id),
    _mbus(m)
{
    ui->setupUi(this);
    lot_id = _datamgr->kilnMachines()->value(kiln_id).lot_id;
    QButtonGroup *mode_group = new QButtonGroup(this);
   mode_group->addButton(ui->pushButton_wb_db);
   mode_group->addButton(ui->pushButton_dep);
   mode_group->addButton(ui->pushButton_emc);
   mode_group->addButton(ui->pushButton_conv);
   setLotValues();
}

DialogSetLotSetup::~DialogSetLotSetup()
{
    delete ui;
}

void DialogSetLotSetup::setLotValues()
{
    LOT lot = _datamgr->lots()->value(lot_id);
    switch (lot.lot_mode) {
    case LOT_MODE::WB_DB :
        ui->pushButton_wb_db->setChecked(true);
        break;

    case LOT_MODE::EMC :
        ui->pushButton_emc->setChecked(true);
        break;

    case LOT_MODE::DEP :
        ui->pushButton_dep->setChecked(true);
        break;

    case LOT_MODE::CONV :
        ui->pushButton_conv->setChecked(true);
        break;
    }
}

void DialogSetLotSetup::setLotParameters()
{
    LOT lot = _datamgr->lots()->value(lot_id);
    if(ui->pushButton_wb_db->isChecked())
        lot.lot_mode = LOT_MODE::WB_DB;
    else if(ui->pushButton_dep->isChecked())
        lot.lot_mode = LOT_MODE::DEP;
    else if(ui->pushButton_emc->isChecked())
        lot.lot_mode = LOT_MODE::EMC;
    else
        lot.lot_mode = LOT_MODE::CONV;

    _datamgr->updateLot(lot);
}

void DialogSetLotSetup::on_pushButton_cancel_clicked()
{
    this->close();
}

void DialogSetLotSetup::on_pushButton_apply_clicked()
{
    this->setLotParameters();
}

void DialogSetLotSetup::on_pushButton_ok_clicked()
{
    this->setLotParameters();
    _mbus->setMode(_datamgr->lots()->value(lot_id).lot_mode);
    this->close();
}

void DialogSetLotSetup::on_pushButton_schedule_clicked()
{
     DialogSchedule *dlg = new DialogSchedule(_datamgr,kiln_id);
    if(!dlg->exec());
}
