#include "datamanager.h"

dataManager::dataManager(QObject *parent) : QObject(parent)
{
    main_data = NULL;
    kiln_machines = NULL;
    _lots = NULL;
}

void dataManager::setMainData(MAIN_DATA md)
{
    if(main_data == NULL)
        main_data = new MAIN_DATA;
    main_data->kiln_count = md.kiln_count;
    main_data->lot_count = md.lot_count;
    main_data->total_lots_count = md.total_lots_count;
}


void dataManager::updateMainData(MAIN_DATA md)
{
    if(main_data == NULL)
        main_data = new MAIN_DATA;
    main_data->kiln_count = md.kiln_count;
    main_data->lot_count = md.lot_count;
    Q_EMIT(mainDataUpdated(*main_data));
}


MAIN_DATA* dataManager::mainData()
{
    return main_data;
}

void dataManager::setKilnTable(QMap<int,KILN> kilns)
{
    if(kiln_machines == NULL)
        kiln_machines = new QMap<int,KILN>;
    kiln_machines->clear();
    *kiln_machines = kilns;
}

void dataManager::updateKilnMachine(KILN kiln)
{
    kiln_machines->insert(kiln.id,kiln);
    Q_EMIT(kilnMachineUpdated(kiln));
}

void dataManager::addNewKilnMachine(KILN kiln)
{
    kiln_machines->insert(kiln.id,kiln);
    Q_EMIT(kilnMachineAdded(kiln));
}

void dataManager::removeKilnMachine(KILN kiln)
{
    kiln_machines->remove(kiln.id);
    Q_EMIT(kilnMachineRemoved(kiln));
}


void dataManager::setLotsTable(QMap<int,LOT> lots)
{
    if(_lots == NULL)
        _lots = new QMap<int,LOT>;
    _lots->clear();
    *_lots = lots;
}

void dataManager::updateLot(LOT lot)
{
    _lots->insert(lot.id,lot);    
    Q_EMIT(lotUpdated(lot));
}

void dataManager::addNewLot(LOT lot)
{
    _lots->insert(lot.id,lot);
    Q_EMIT(lotAdded(lot));
}

void dataManager::removeLot(LOT lot)
{
    _lots->remove(lot.id);
    Q_EMIT(lotRemoved(lot));
}

QMap<int,KILN> *dataManager::kilnMachines()
{
    return kiln_machines;
}

QMap<int,LOT> *dataManager::lots()
{
    return _lots;
}
