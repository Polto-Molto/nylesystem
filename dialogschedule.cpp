#include "dialogschedule.h"
#include "ui_dialogschedule.h"

DialogSchedule::DialogSchedule(dataManager *d,int id,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogSchedule),
    _datamgr(d),
    kiln_id(id)
{
    ui->setupUi(this);
    lot_id = _datamgr->kilnMachines()->value(kiln_id).lot_id;

    row[0].insert(0,ui->lineEdit_wb);
    row[0].insert(1,ui->lineEdit_db);
    row[0].insert(2,ui->lineEdit_dep);
    row[0].insert(3,ui->lineEdit_emc);
    row[0].insert(4,ui->lineEdit_fan);
    row[0].insert(5,ui->lineEdit_hours);

    row[1].insert(0,ui->lineEdit_wb_2);
    row[1].insert(1,ui->lineEdit_db_2);
    row[1].insert(2,ui->lineEdit_dep_2);
    row[1].insert(3,ui->lineEdit_emc_2);
    row[1].insert(4,ui->lineEdit_fan_2);
    row[1].insert(5,ui->lineEdit_hours_2);

    row[2].insert(0,ui->lineEdit_wb_3);
    row[2].insert(1,ui->lineEdit_db_3);
    row[2].insert(2,ui->lineEdit_dep_3);
    row[2].insert(3,ui->lineEdit_emc_3);
    row[2].insert(4,ui->lineEdit_fan_3);
    row[2].insert(5,ui->lineEdit_hours_3);

    row[3].insert(0,ui->lineEdit_wb_4);
    row[3].insert(1,ui->lineEdit_db_4);
    row[3].insert(2,ui->lineEdit_dep_4);
    row[3].insert(3,ui->lineEdit_emc_4);
    row[3].insert(4,ui->lineEdit_fan_4);
    row[3].insert(5,ui->lineEdit_hours_4);

    row[4].insert(0,ui->lineEdit_wb_5);
    row[4].insert(1,ui->lineEdit_db_5);
    row[4].insert(2,ui->lineEdit_dep_5);
    row[4].insert(3,ui->lineEdit_emc_5);
    row[4].insert(4,ui->lineEdit_fan_5);
    row[4].insert(5,ui->lineEdit_hours_5);

    row[5].insert(0,ui->lineEdit_wb_6);
    row[5].insert(1,ui->lineEdit_db_6);
    row[5].insert(2,ui->lineEdit_dep_6);
    row[5].insert(3,ui->lineEdit_emc_6);
    row[5].insert(4,ui->lineEdit_fan_6);
    row[5].insert(5,ui->lineEdit_hours_6);

    row[6].insert(0,ui->lineEdit_wb_7);
    row[6].insert(1,ui->lineEdit_db_7);
    row[6].insert(2,ui->lineEdit_dep_7);
    row[6].insert(3,ui->lineEdit_emc_7);
    row[6].insert(4,ui->lineEdit_fan_7);
    row[6].insert(5,ui->lineEdit_hours_7);

    row[7].insert(0,ui->lineEdit_wb_8);
    row[7].insert(1,ui->lineEdit_db_8);
    row[7].insert(2,ui->lineEdit_dep_8);
    row[7].insert(3,ui->lineEdit_emc_8);
    row[7].insert(4,ui->lineEdit_fan_8);
    row[7].insert(5,ui->lineEdit_hours_8);
    loadData();
}

DialogSchedule::~DialogSchedule()
{
    delete ui;
}

void DialogSchedule::on_pushButton_load_clicked()
{

}

void DialogSchedule::on_pushButton_save_clicked()
{
    this->saveData();
}

void DialogSchedule::on_pushButton_cancel_clicked()
{
    this->close();
}

void DialogSchedule::on_pushButton_enabled_clicked()
{

}

void DialogSchedule::on_pushButton_disabled_clicked()
{

}

void DialogSchedule::loadData()
{
    LOT lot = _datamgr->lots()->value(lot_id);
    for(int i=0 ;i < 8 ; i++)
    {
        row[i].value(0)->setText(QString::number(lot.lot_schedule[i].wb));
        row[i].value(1)->setText(QString::number(lot.lot_schedule[i].db));
        row[i].value(2)->setText(QString::number(lot.lot_schedule[i].dep));
        row[i].value(3)->setText(QString::number(lot.lot_schedule[i].emc));
        row[i].value(4)->setText(QString::number(lot.lot_schedule[i].fan_speed));
        row[i].value(5)->setText(QString::number(lot.lot_schedule[i].left_hours));
    }
}

void DialogSchedule::saveData()
{
    LOT lot = _datamgr->lots()->value(lot_id);
    for(int i=0 ;i < 8 ; i++)
    {
        lot.lot_schedule[i].wb          =  row[i].value(0)->text().toInt();
        lot.lot_schedule[i].db          =  row[i].value(1)->text().toInt();
        lot.lot_schedule[i].dep         =  row[i].value(2)->text().toInt();
        lot.lot_schedule[i].emc         =  row[i].value(3)->text().toInt();
        lot.lot_schedule[i].fan_speed   =  row[i].value(4)->text().toInt();
        lot.lot_schedule[i].left_hours  =  row[i].value(5)->text().toInt();
    }
    _datamgr->updateLot(lot);
}
