#ifndef DIALOGSCHEDULE_H
#define DIALOGSCHEDULE_H

#include <QDialog>
#include <QLineEdit>
#include "datamanager.h"

namespace Ui {
class DialogSchedule;
}

class DialogSchedule : public QDialog
{
    Q_OBJECT

public:
    explicit DialogSchedule(dataManager*,int,QWidget *parent = 0);
    ~DialogSchedule();
signals:

public slots:
    void on_pushButton_load_clicked();
    void on_pushButton_save_clicked();
    void on_pushButton_cancel_clicked();
    void on_pushButton_enabled_clicked();
    void on_pushButton_disabled_clicked();

    void loadData();
    void saveData();

private:
    Ui::DialogSchedule *ui;
    QMap<int,QLineEdit*> row[8];
    dataManager *_datamgr;
    int lot_id;
    int kiln_id;
};

#endif // DIALOGSCHEDULE_H
