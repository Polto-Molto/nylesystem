#ifndef MAINSCREEN_H
#define MAINSCREEN_H

#include <QWidget>
#include "datatypes.h"
#include "datamanager.h"
#include "dialognewlotselectkiln.h"
#include "dialogsetkilnequipment.h"
#include "dialogsetlotsetup.h"
#include "modbus_action.h"

namespace Ui {
class mainscreen;
}

class mainscreen : public QWidget
{
    Q_OBJECT

public:
    explicit mainscreen(dataManager*,QWidget *parent = 0);
    ~mainscreen();

signals:
      void touched(int);

public slots:
    void initialize_system();
    void setCurrentKiln(int);

    void on_pushButton_new_lot_clicked();
    void on_pushButton_equip_clicked();
    void on_pushButton_setup_clicked();

private:
    Ui::mainscreen *ui;
    dataManager* _datamgr;
    KILN current_kiln;
    modbus_action* _mbus;
};

#endif // MAINSCREEN_H
