#ifndef DATAMANAGER_H
#define DATAMANAGER_H

#include <QObject>
#include "datatypes.h"
#include <QMap>

class dataManager : public QObject
{
    Q_OBJECT
public:
    explicit dataManager(QObject *parent = 0);

signals:
    void mainDataUpdated(MAIN_DATA);

    void kilnMachineAdded(KILN);
    void kilnMachineUpdated(KILN);
    void kilnMachineRemoved(KILN);

    void lotAdded(LOT);
    void lotUpdated(LOT);
    void lotRemoved(LOT);

public slots:
    void setMainData(MAIN_DATA);
    void updateMainData(MAIN_DATA);
    MAIN_DATA* mainData();

    void setKilnTable(QMap<int,KILN>);
    void updateKilnMachine(KILN);
    void addNewKilnMachine(KILN);
    void removeKilnMachine(KILN);

    void setLotsTable(QMap<int,LOT>);
    void updateLot(LOT);
    void addNewLot(LOT);
    void removeLot(LOT);

    QMap<int,KILN> *kilnMachines();
    QMap<int,LOT> *lots();

private:
    MAIN_DATA *main_data;
    QMap<int,KILN> *kiln_machines;
    QMap<int,LOT> *_lots;
};

#endif // DATAMANAGER_H
