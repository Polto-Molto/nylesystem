#include "kilnselectionitem.h"
#include "ui_kilnselectionitem.h"

kilnSelectionItem::kilnSelectionItem(KILN k,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::kilnSelectionItem),
    _kiln(k)
{
    ui->setupUi(this);
    selection = new QPushButton(this);
    selection->setGeometry(this->geometry());
    selection->setFlat(true);
    selection->setCheckable(true);
    selection->setObjectName("selection");
    selection->show();
    selection->setStyleSheet("\
                QPushButton#selection{background-color:rgba(0, 0, 0, 0%);border:0px;border-radius:10px;} \
                QPushButton#selection:checked{background-color:rgba(0, 0, 0, 50%);} \
                QPushButton#selection:pressed {background-color:rgba(0,0,0,30%);}");
    selection->setWindowOpacity(0);
    updateStatus(k);
    connect(selection,SIGNAL(clicked(bool)),this,SLOT(selectedKiln()));
}

kilnSelectionItem::~kilnSelectionItem()
{
    delete ui;
}

void kilnSelectionItem::selectedKiln()
{
    Q_EMIT(selected(_kiln));
}

void kilnSelectionItem::updateStatus(KILN k)
{
    _kiln = k;
    ui->label_id->setText(QString::number(_kiln.id));
    ui->label_modbus_id->setText(QString::number(_kiln.modbus_id));
    QString status;
    switch(_kiln.equipment.operate_status)
    {
        case OPERATE_STATUS::OFF :
            status = "FREE";
            break;

        case OPERATE_STATUS::ON :
           status = "BUSY";
            break;

        case OPERATE_STATUS::PUASE :
           status = "PUASE";
            break;
    }

    ui->label_status->setText(status);
}
