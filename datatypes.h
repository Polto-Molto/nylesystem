#ifndef DATATYPES_H
#define DATATYPES_H

/////////////////////////////////////////////////////////////////
/// Data Types of LOT Structur
/////////////////////////////////////////////////////////////////

enum LOT_MODE
{
    WB_DB,
    EMC,
    DEP,
    CONV
};

struct LOT_SCHEDULE
{
    int wb;
    int db;
    int dep;
    int emc;
    int fan_speed;
    int left_hours;
    int pass_hours;
};


struct LOT
{
    int id;
    LOT_MODE lot_mode;
    LOT_SCHEDULE lot_schedule[8];
    LOT()
    {
        id = 0;
        lot_mode = LOT_MODE::CONV;
        for(int i=0;i<8;i++)
        {
            lot_schedule[i].db = 0;
            lot_schedule[i].dep = 0;
            lot_schedule[i].emc = 0;
            lot_schedule[i].fan_speed = 0;
            lot_schedule[i].left_hours = 0;
            lot_schedule[i].pass_hours = 0;
            lot_schedule[i].wb = 0;
        }
    }
};

/////////////////////////////////////////////////////////////////
/// Data Types of KILN Structur
/////////////////////////////////////////////////////////////////
enum class OPERATE_STATUS
{
    OFF,
    ON,
    PUASE
};

enum class OPERATION
{
    AUTO,
    MANUAL,
    OFF
};

enum DIRECTION
{
    REVERSE,
    FORWARD
};

struct FAN_DIRECTION
{
    DIRECTION direction;
    OPERATION opreration;
    int reverse_hours;
    int forward_hours;
};

struct FAN_SPEED
{
    OPERATION operation;
    int value;
};

struct DRIVE_INFO
{
    int current;
    int rpm;
    int hz;
};

struct VENTS
{
    OPERATION operation;
    int value;
};

struct SPRAY
{
    OPERATION operation;
    int value;
};

struct COMPRESSOR
{
    OPERATION operation;
    int suction;
    int discharge;
    int slt;
    int slp;
    int dlp;
    int dlt;
    int oil;
    int call;
    int ctd;
};

struct EQUIPMENT
{
    OPERATE_STATUS  operate_status;
    FAN_DIRECTION   fan_direction;
    FAN_SPEED       fan_speed;
    VENTS           vents;
    SPRAY           spray;
    DRIVE_INFO      drive_info;
    COMPRESSOR      compressor;
};


struct KILN
{
    int id;
    int modbus_id;
    int lot_id;
    EQUIPMENT equipment;
    KILN()
    {
        id = 0;
        modbus_id = 0;
        lot_id = 0;
        equipment.operate_status = OPERATE_STATUS::OFF;

        equipment.fan_direction.opreration = OPERATION::OFF;
        equipment.fan_direction.direction = DIRECTION::FORWARD;
        equipment.fan_direction.forward_hours = 0;
        equipment.fan_direction.reverse_hours = 0;

        equipment.fan_speed.operation = OPERATION::OFF;
        equipment.fan_speed.value = 0;

        equipment.vents.operation = OPERATION::OFF;
        equipment.vents.value = 0;

        equipment.spray.operation = OPERATION::OFF;
        equipment.spray.value = 0;

        equipment.drive_info.current = 0;
        equipment.drive_info.rpm = 0;
        equipment.drive_info.hz = 0;

        equipment.compressor.operation = OPERATION::OFF;
        equipment.compressor.call = 0;
        equipment.compressor.ctd = 0;
        equipment.compressor.discharge = 0;
        equipment.compressor.dlp = 0;
        equipment.compressor.dlt = 0;
        equipment.compressor.oil = 0;
        equipment.compressor.slp = 0;
        equipment.compressor.slt = 0;
        equipment.compressor.suction = 0;

    }
};

/////////////////////////////////////////////////////////////////
/// Data Types of MAIN DATA Structur
/////////////////////////////////////////////////////////////////

struct MAIN_DATA
{
    int kiln_count;
    int lot_count;
    int total_lots_count;
    MAIN_DATA()
    {
        kiln_count = 0;
        lot_count = 0;
        total_lots_count = 0;
    }
};


#define COLOR_AUTO  "background-color: rgb(0, 218, 0);"
#define COLOR_MANUAL  "background-color: rgb(218, 218, 0);"
#define COLOR_OFF  "background-color: rgb(218, 0, 0);"

#endif // DATATYPES_H
