#ifndef KILNSELECTIONITEM_H
#define KILNSELECTIONITEM_H

#include <QWidget>
#include <QPushButton>
#include "datatypes.h"

namespace Ui {
class kilnSelectionItem;
}

class kilnSelectionItem : public QWidget
{
    Q_OBJECT

public:
    explicit kilnSelectionItem(KILN,QWidget *parent = 0);
    ~kilnSelectionItem();

signals:
    void selected(KILN);

public slots:
    void updateStatus(KILN k);
    void selectedKiln();

private:
    Ui::kilnSelectionItem *ui;
    QPushButton *selection;
    KILN _kiln;
};

#endif // KILNSELECTIONITEM_H
