#ifndef SCREEN_2_H
#define SCREEN_2_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_2;
}

class screen_2 : public QWidget
{
    Q_OBJECT

public:
    explicit screen_2(dataManager*,QWidget *parent = 0);
    ~screen_2();

private:
    Ui::screen_2 *ui;
    dataManager* _datamgr;
};

#endif // SCREEN_2_H
