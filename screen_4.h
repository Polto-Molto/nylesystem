#ifndef SCREEN_4_H
#define SCREEN_4_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_4;
}

class screen_4 : public QWidget
{
    Q_OBJECT

public:
    explicit screen_4(dataManager*,QWidget *parent = 0);
    ~screen_4();

private:
    Ui::screen_4 *ui;
    dataManager* _datamgr;
};

#endif // SCREEN_4_H
