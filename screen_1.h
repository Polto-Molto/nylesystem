#ifndef SCREEN_1_H
#define SCREEN_1_H

#include <QWidget>
#include "datamanager.h"
#include "dialognewlotselectkiln.h"
#include "dialogsetkilnequipment.h"

namespace Ui {
class screen_1;
}

class screen_1 : public QWidget
{
    Q_OBJECT

public:
    explicit screen_1(dataManager*,QWidget *parent = 0);
    ~screen_1();
signals:
      void touched(int);

public slots:
    void on_pushButton_new_lot_clicked();
    void on_pushButton_equip_clicked();

private:
    Ui::screen_1 *ui;
    dataManager* _datamgr;
    int current_kiln_id;
};

#endif // SCREEN_1_H
