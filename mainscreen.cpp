#include "mainscreen.h"
#include "ui_mainscreen.h"
#include <QDebug>

mainscreen::mainscreen(dataManager* d,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::mainscreen),
    _datamgr(d)
{
    ui->setupUi(this);
    _mbus = new modbus_action(1);
    qDebug() << _mbus->connectToModbus("127.0.0.1",502);

    initialize_system();
    connect(ui->comboBox,SIGNAL(currentIndexChanged(int))
            ,this,SLOT(setCurrentKiln(int)));

    if(ui->comboBox->count() > 0)
    {
        ui->comboBox->setCurrentIndex(0);
        setCurrentKiln(0);
    }
}

mainscreen::~mainscreen()
{
    delete ui;
}

void mainscreen::setCurrentKiln(int i)
{
    current_kiln = _datamgr->kilnMachines()->value(i+1);
    LOT lot = _datamgr->lots()->value(current_kiln.lot_id);
    switch (current_kiln.equipment.spray.operation)
    {
        case OPERATION::AUTO :
            ui->label_spray->setText("AUTO");
            ui->label_spray->setStyleSheet(COLOR_AUTO);
            break;
        case OPERATION::MANUAL :
            ui->label_spray->setText("ENABLED");
            ui->label_spray->setStyleSheet(COLOR_MANUAL);
            break;
        case OPERATION::OFF :
            ui->label_spray->setText("DISABLED");
            ui->label_spray->setStyleSheet(COLOR_OFF);
            break;
    }

    switch (current_kiln.equipment.fan_direction.opreration)
    {
        case OPERATION::AUTO :
            ui->label_fan->setText("AUTO");
            ui->label_fan->setStyleSheet(COLOR_AUTO);
            break;
        case OPERATION::MANUAL :
            ui->label_fan->setText("ENABLED");
            ui->label_fan->setStyleSheet(COLOR_MANUAL);
            break;
        case OPERATION::OFF :
            ui->label_fan->setText("DISABLED");
            ui->label_fan->setStyleSheet(COLOR_OFF);
            break;
    }

    switch (current_kiln.equipment.vents.operation)
    {
        case OPERATION::AUTO :
            ui->label_vent->setText("AUTO");
            ui->label_vent->setStyleSheet(COLOR_AUTO);
            break;
        case OPERATION::MANUAL :
            ui->label_vent->setText("ENABLED");
            ui->label_vent->setStyleSheet(COLOR_MANUAL);
            break;
        case OPERATION::OFF :
            ui->label_vent->setText("DISABLED");
            ui->label_vent->setStyleSheet(COLOR_OFF);
            break;
    }

}

void mainscreen::initialize_system()
{
    foreach (KILN kiln, _datamgr->kilnMachines()->values()) {
           ui->comboBox->addItem("KILN_"+QString::number(kiln.id));
       }
}

void mainscreen::on_pushButton_new_lot_clicked()
{
    int id =0;
    DialogNewLotSelectKiln *dlg = new DialogNewLotSelectKiln(_datamgr);
    if(!dlg->exec())
        id =  dlg->currentKilnID();
    if(id > 0)
        setCurrentKiln(id);
}

void mainscreen::on_pushButton_equip_clicked()
{
    DialogSetKilnEquipment *dlg = new DialogSetKilnEquipment(_datamgr,current_kiln.id,_mbus);
    if(!dlg->exec());
}

void mainscreen::on_pushButton_setup_clicked()
{
    DialogSetLotSetup *dlg = new DialogSetLotSetup(_datamgr,current_kiln.id,_mbus);
    if(!dlg->exec());
}
