#include "dialognewlotselectkiln.h"
#include "ui_dialognewlotselectkiln.h"
#include <QDebug>

DialogNewLotSelectKiln::DialogNewLotSelectKiln(dataManager* d,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogNewLotSelectKiln),
    _datamgr(d)
{
    ui->setupUi(this);
    ui->stackedWidget->setCurrentIndex(0);
    ui->pushButton_create->hide();
    getFreeKiln();
    if(free_kilns.isEmpty())
        ui->label_status->setText("No Free Machines For Your Lot Now\nCheck Later ...");
    else
    {
        ui->label_status->setText("New LOT ID IS : " + QString::number(getNewLotID()));
        ui->pushButton_create->show();
    }
}

DialogNewLotSelectKiln::~DialogNewLotSelectKiln()
{
    foreach (kilnSelectionItem* item, free_kilns)
        delete item;
    free_kilns.clear();
    delete ui;
}

void DialogNewLotSelectKiln::getFreeKiln()
{
    foreach (kilnSelectionItem* item, free_kilns)
        delete item;
    free_kilns.clear();
    int i=0;
    foreach (KILN kiln, _datamgr->kilnMachines()->values())
    {
        if(kiln.equipment.operate_status == OPERATE_STATUS::OFF)
        {
            kilnSelectionItem *item = new kilnSelectionItem(kiln);
            connect(item,SIGNAL(selected(KILN)),this, SLOT(setCurrentKiln(KILN)));
            ui->gridLayout->addWidget(item,i/3,i%3);
            free_kilns.push_back(item);
            i++;
        }
    }
}

int DialogNewLotSelectKiln::getNewLotID()
{
    return _datamgr->mainData()->total_lots_count +1;
}

void DialogNewLotSelectKiln::on_pushButton_cancel_clicked()
{
    this->close();
}

void DialogNewLotSelectKiln::on_pushButton_create_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
}

void DialogNewLotSelectKiln::on_pushButton_cancel_kiln_clicked()
{
    this->close();
}

void DialogNewLotSelectKiln::on_pushButton_finish_clicked()
{
    MAIN_DATA *md = _datamgr->mainData();
    md->total_lots_count ++;
    LOT lot;
    lot.id = md->total_lots_count;
    current_kiln.lot_id = lot.id;
    current_kiln.equipment.operate_status = OPERATE_STATUS::ON;
    _datamgr->updateKilnMachine(current_kiln);
    _datamgr->updateMainData(*md);
    _datamgr->addNewLot(lot);
    this->close();
}

void DialogNewLotSelectKiln::setCurrentKiln(KILN kiln)
{
    current_kiln = kiln;
}

int DialogNewLotSelectKiln::currentKilnID()
{
    return current_kiln.id;
}
