#ifndef CUSTOMER_H
#define CUSTOMER_H

#include <QWidget>
#include <QtSql>

namespace Ui {
class customer;
}

class customer : public QWidget
{
    Q_OBJECT

public:
    enum OPERATION{
        ADD_NEW,
        UPDATE
    };

public:
    explicit customer(QSqlDatabase*,OPERATION,QWidget *parent = 0);
    ~customer();
public slots:
    void on_pushButton_add_clicked();
    void on_pushButton_close_clicked();
    void on_pushButton_update_clicked();
    void on_pushButton_remove_clicked();

    void loadDatabase();
    void loadItemSettings();
    void clearForm();

private:
    Ui::customer *ui;
    QSqlDatabase *_db;
};

#endif // CUSTOMER_H
