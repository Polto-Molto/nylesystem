#ifndef DBWINDOW_H
#define DBWINDOW_H

#include <QMainWindow>
#include <QtSql>
#include <QDebug>

#include "customer.h"
#include "kiln.h"
#include "service.h"
#include "psql_settings.h"

namespace Ui {
class dbWindow;
}

class dbWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit dbWindow(QWidget *parent = 0);
    ~dbWindow();
public slots:
    void connectdb();

    void open_addNew_customer();
    void open_update_customer();

    void open_addNew_kiln();
    void open_update_kiln();

    void open_addNew_service();
    void open_update_service();

    void open_psql_settings_dlg();

private:
    Ui::dbWindow *ui;
    QSqlDatabase db;
    customer *customer_dlg;
    kiln *kiln_dlg;
    Service *Service_dlg;
    psql_settings *psql_settings_dlg;
};

#endif // DBWINDOW_H
