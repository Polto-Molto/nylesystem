#ifndef PSQL_SETTINGS_H
#define PSQL_SETTINGS_H

#include <QWidget>
#include <QtSql>
#include <QDebug>

namespace Ui {
class psql_settings;
}

class psql_settings : public QWidget
{
    Q_OBJECT

public:
    explicit psql_settings(QSqlDatabase*,QWidget *parent = 0);
    ~psql_settings();

public slots:
    void on_pushButton_cancel_clicked();
    void on_pushButton_save_clicked();
    void on_pushButton_test_connection_clicked();

private:
    Ui::psql_settings *ui;
    QSqlDatabase *_db;
};

#endif // PSQL_SETTINGS_H
