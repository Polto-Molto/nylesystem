#include "dbwindow.h"
#include "ui_dbwindow.h"

dbWindow::dbWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::dbWindow)
{
    ui->setupUi(this);
    db = QSqlDatabase::addDatabase("QPSQL","base");
    connectdb();

    customer_dlg = 0;
    kiln_dlg = 0;
    Service_dlg = 0;
    psql_settings_dlg = 0;
    connect(ui->actionDatabase_Connection_setting,SIGNAL(triggered(bool)),
            this,SLOT(open_psql_settings_dlg()));

    connect(ui->actionCustomer_new,SIGNAL(triggered(bool)),
            this,SLOT(open_addNew_customer()));

    connect(ui->actionCustomer_update,SIGNAL(triggered(bool)),
            this,SLOT(open_update_customer()));

    connect(ui->actionKiln_new,SIGNAL(triggered(bool)),
            this,SLOT(open_addNew_kiln()));

    connect(ui->actionKiln_update,SIGNAL(triggered(bool)),
            this,SLOT(open_update_kiln()));

    connect(ui->actionService_new,SIGNAL(triggered(bool)),
            this,SLOT(open_addNew_service()));

    connect(ui->actionService_update,SIGNAL(triggered(bool)),
            this,SLOT(open_update_service()));

    connect(ui->actionexit,SIGNAL(triggered(bool)),
             this,SLOT(close()));
}

dbWindow::~dbWindow()
{
    delete ui;
}

void dbWindow::connectdb()
{
    db.setHostName("127.0.0.1");
    db.setDatabaseName("nyledb");
    db.setUserName("nylesystem");
    db.setPassword("nyle");
     if(db.open())
         qDebug() << "database connected ...";
     else
         qDebug() << "Eroor database connecting ...";

}

void dbWindow::open_psql_settings_dlg()
{
    psql_settings_dlg = new psql_settings(&db);
    psql_settings_dlg->show();
}

void dbWindow::open_addNew_customer()
{
    customer_dlg = new customer(&db,customer::ADD_NEW);
    customer_dlg->show();
}

void dbWindow::open_update_customer()
{
    customer_dlg = new customer(&db,customer::UPDATE);
    customer_dlg->show();
}

void dbWindow::open_addNew_kiln()
{
    kiln_dlg = new kiln(&db,kiln::ADD_NEW);
    kiln_dlg->show();
}

void dbWindow::open_update_kiln()
{
    kiln_dlg = new kiln(&db,kiln::UPDATE);
    kiln_dlg->show();
}

void dbWindow::open_addNew_service()
{
    Service_dlg = new Service(&db,Service::ADD_NEW);
    Service_dlg->show();
}

void dbWindow::open_update_service()
{
    Service_dlg = new Service(&db,Service::UPDATE);
    Service_dlg->show();
}
