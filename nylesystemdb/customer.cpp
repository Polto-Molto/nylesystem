#include "customer.h"
#include "ui_customer.h"

customer::customer(QSqlDatabase* db,OPERATION o,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::customer)
{
    ui->setupUi(this);
    _db = db;
    ui->lineEdit_id->hide();
    ui->comboBox_id->hide();
    ui->pushButton_add->hide();
    ui->pushButton_remove->hide();
    ui->pushButton_update->hide();

    if(o == OPERATION::ADD_NEW)
    {
        ui->lineEdit_id->show();
        ui->pushButton_add->show();
    }
    else {
       loadDatabase();
       ui->comboBox_id->show();
       ui->pushButton_remove->show();
       ui->pushButton_update->show();
       connect(ui->comboBox_id,SIGNAL(currentIndexChanged(int)),this,SLOT(loadItemSettings()));
    }
}

customer::~customer()
{
    delete ui;
}

void customer::clearForm()
{
    ui->lineEdit_id->clear();
    ui->lineEdit_email->clear();
    ui->lineEdit_first_name->clear();
    ui->lineEdit_last_name->clear();
    ui->lineEdit_latitude->clear();
    ui->lineEdit_longitude->clear();
    ui->lineEdit_phone->clear();
}

void customer::loadDatabase()
{
    ui->comboBox_id->clear();
    QSqlQuery query(*_db);
    QList<int> id_list;
    query.exec("SELECT id ,lname ,fname,lat,long,email,phone FROM customer");
    while(query.next())
        id_list << query.value(0).toInt();
    foreach(int id ,id_list)
        ui->comboBox_id->addItem(QString::number(id));
    if(!id_list.isEmpty())
        loadItemSettings();
}

void customer::loadItemSettings()
{
    if(ui->comboBox_id->count() > 0)
    {
        QSqlQuery query(*_db);

        query.prepare("SELECT id ,lname ,fname,lat,long,email,phone FROM customer WHERE id = :id ");
        query.bindValue(":id", ui->comboBox_id->currentText().toInt());
        query.exec();

        while(query.next())
        {
            ui->lineEdit_last_name->setText(query.value(1).toString());
            ui->lineEdit_first_name->setText(query.value(2).toString());
            ui->lineEdit_latitude->setText(query.value(3).toString());
            ui->lineEdit_longitude->setText(query.value(4).toString());
            ui->lineEdit_email->setText(query.value(5).toString());
            ui->lineEdit_phone->setText(query.value(6).toString());
        }
        ui->label_status->setText("");
    }
}

void customer::on_pushButton_add_clicked()
{
    QSqlQuery query(*_db);

    query.prepare("INSERT INTO customer (id ,lname ,fname,lat,long,email,phone) Values   \
                  (:id ,:lname ,:fname,:lat,:long,:email,:phone)  ");

    query.bindValue(":id"    , ui->lineEdit_id->text().toInt());
    query.bindValue(":lname" , ui->lineEdit_last_name->text());
    query.bindValue(":fname" ,ui->lineEdit_first_name->text());
    query.bindValue(":lat"   ,ui->lineEdit_latitude->text().toDouble());
    query.bindValue(":long"  ,ui->lineEdit_longitude->text().toDouble());
    query.bindValue(":email" ,ui->lineEdit_email->text());
    query.bindValue(":phone" ,ui->lineEdit_phone->text());
    if(query.exec()){
        ui->label_status->setText("Record Added ...");
        clearForm();
    }
}

void customer::on_pushButton_close_clicked()
{
    delete this;
}

void customer::on_pushButton_update_clicked()
{
    QSqlQuery query(*_db);

    query.prepare("UPDATE customer SET \
                  lname = :lname , fname = :fname , lat = :lat , long = :long \
                  , email = :email  , phone = :phone \
                  WHERE id = :id  ");

    query.bindValue(":lname" , ui->lineEdit_last_name->text());
    query.bindValue(":fname" ,ui->lineEdit_first_name->text());
    query.bindValue(":lat"   ,ui->lineEdit_latitude->text().toDouble());
    query.bindValue(":long"  ,ui->lineEdit_longitude->text().toDouble());
    query.bindValue(":email" ,ui->lineEdit_email->text());
    query.bindValue(":phone" ,ui->lineEdit_phone->text());
    query.bindValue(":id"    , ui->comboBox_id->currentText().toInt());

    if(query.exec())
        ui->label_status->setText("Record Updated ...");
}

void customer::on_pushButton_remove_clicked()
{
    QSqlQuery query(*_db);
    query.prepare("DELETE FROM customer WHERE id = :id ");
    query.bindValue(":id", ui->comboBox_id->currentText().toInt());
    if(query.exec()){
        ui->label_status->setText("Record Deleted ...");
        loadDatabase();
    }
}
