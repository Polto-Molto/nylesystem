#ifndef KILN_H
#define KILN_H

#include <QWidget>
#include <QtSql>

namespace Ui {
class kiln;
}

class kiln : public QWidget
{
    Q_OBJECT
public:
    enum OPERATION{
        ADD_NEW,
        UPDATE
    };

public:
    explicit kiln(QSqlDatabase*,OPERATION,QWidget *parent = 0);
    ~kiln();
public slots:

    void on_pushButton_add_clicked();
    void on_pushButton_close_clicked();
    void on_pushButton_update_clicked();
    void on_pushButton_remove_clicked();

    void loadDatabase();
    void loadItemSettings();
    void clearForm();

private:
    Ui::kiln *ui;
    QSqlDatabase *_db;
    QList<int> customer_id_list;
};

#endif // KILN_H
