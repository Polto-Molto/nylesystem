#include "psql_settings.h"
#include "ui_psql_settings.h"

psql_settings::psql_settings(QSqlDatabase* db,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::psql_settings)
{
    ui->setupUi(this);
    _db = db;
}

psql_settings::~psql_settings()
{
    delete ui;
}

void psql_settings::on_pushButton_cancel_clicked()
{
    delete this;
}

void psql_settings::on_pushButton_save_clicked()
{
    _db->close();
    _db->setHostName(ui->lineEdit_host->text());
    _db->setDatabaseName(ui->lineEdit_dbname->text());
    _db->setUserName(ui->lineEdit_user->text());
    _db->setPassword(ui->lineEdit_pass->text());
    _db->open();
    this->close();
}

void psql_settings::on_pushButton_test_connection_clicked()
{
    QSqlDatabase::removeDatabase("test");
    QSqlDatabase db = QSqlDatabase::addDatabase("QPSQL","test");
    db.setHostName(ui->lineEdit_host->text());
    db.setDatabaseName(ui->lineEdit_dbname->text());
    db.setUserName(ui->lineEdit_user->text());
    db.setPassword(ui->lineEdit_pass->text());
     if(db.open())
        ui->label_status->setText("database connected ...");
    else
        ui->label_status->setText("Eroor database connecting ...");
     db.close();
}
