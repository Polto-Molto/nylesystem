#include "kiln.h"
#include "ui_kiln.h"

kiln::kiln(QSqlDatabase* db,OPERATION o,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::kiln)
{
    ui->setupUi(this);
    _db = db;

    QSqlQuery query(*_db);

    query.exec("SELECT id FROM customer");
    while(query.next())
        customer_id_list << query.value(0).toInt();
    foreach(int id ,customer_id_list)
        ui->comboBox_customer_id->addItem(QString::number(id));

    ui->pushButton_add->hide();
    ui->pushButton_remove->hide();
    ui->pushButton_update->hide();
    ui->comboBox_id->hide();
    ui->lineEdit_kiln_id->hide();

    if(o == OPERATION::ADD_NEW)
    {
        ui->pushButton_add->show();
        ui->lineEdit_kiln_id->show();
    }
    else {
       loadDatabase();
       ui->comboBox_id->show();
       ui->pushButton_remove->show();
       ui->pushButton_update->show();
       connect(ui->comboBox_id,SIGNAL(currentIndexChanged(int)),this,SLOT(loadItemSettings()));
    }
}

kiln::~kiln()
{
    delete ui;
}


void kiln::clearForm()
{
    ui->lineEdit_kiln_id->clear();
    ui->lineEdit_kiln_type->clear();
    ui->lineEdit_serial_number->clear();
}

void kiln::loadDatabase()
{
    ui->comboBox_id->clear();

    QSqlQuery query(*_db);
    QList<int> kiln_id_list;
    query.exec("SELECT id FROM kiln");
    while(query.next())
        kiln_id_list << query.value(0).toInt();
    foreach(int id ,kiln_id_list)
        ui->comboBox_id->addItem(QString::number(id));
    if(!kiln_id_list.isEmpty())
        loadItemSettings();

}

void kiln::loadItemSettings()
{
    if(ui->comboBox_id->count() > 0)
    {
        QSqlQuery query(*_db);

        query.prepare("SELECT customer_id,type,serial FROM kiln WHERE id = :id ");
        query.bindValue(":id", ui->comboBox_id->currentText().toInt());
        query.exec();

        while(query.next())
        {
            ui->comboBox_customer_id->setCurrentText(query.value(0).toString());
            ui->lineEdit_kiln_type->setText(query.value(1).toString());
            ui->lineEdit_serial_number->setText(query.value(2).toString());
        }
        ui->label_status->setText("");
    }
}

void kiln::on_pushButton_add_clicked()
{
    QSqlQuery query(*_db);

    query.prepare("INSERT INTO kiln (id ,customer_id ,type,serial) Values   \
                  (:id ,:customer_id ,:type,:serial)  ");

    query.bindValue(":id"    , ui->lineEdit_kiln_id->text().toInt());
    query.bindValue(":customer_id" , ui->comboBox_customer_id->currentText().toInt());
    query.bindValue(":type" ,ui->lineEdit_kiln_type->text().toInt());
    query.bindValue(":serial"   ,ui->lineEdit_serial_number->text().toInt());
    if(query.exec()){
        ui->label_status->setText("Record Added ...");
        clearForm();
    }
}

void kiln::on_pushButton_close_clicked()
{
    delete this;
}

void kiln::on_pushButton_update_clicked()
{
    QSqlQuery query(*_db);

    query.prepare("UPDATE kiln SET \
                  customer_id  = :customer_id ,type = :type ,serial = :serial \
                  WHERE id = :id  ");

            query.bindValue(":customer_id" , ui->comboBox_customer_id->currentText().toInt());
            query.bindValue(":type" ,ui->lineEdit_kiln_type->text().toInt());
            query.bindValue(":serial"   ,ui->lineEdit_serial_number->text().toInt());
            query.bindValue(":id"    , ui->comboBox_id->currentText().toInt());

    if(query.exec())
        ui->label_status->setText("Record Updated ...");
}

void kiln::on_pushButton_remove_clicked()
{
    QSqlQuery query(*_db);
    query.prepare("DELETE FROM kiln WHERE id = :id ");
    query.bindValue(":id", ui->comboBox_id->currentText().toInt());
    if(query.exec()){
        ui->label_status->setText("Record Deleted ...");
        loadDatabase();
    }
}
