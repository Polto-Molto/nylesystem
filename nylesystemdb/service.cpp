#include "service.h"
#include "ui_service.h"

Service::Service(QSqlDatabase* db,OPERATION o,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Service)
{
    ui->setupUi(this);
    _db = db;
    ui->pushButton_add->hide();
    ui->pushButton_remove->hide();
    ui->pushButton_update->hide();
    ui->comboBox_serial_number->hide();
    ui->lineEdit_serial_number->hide();

    if(o == OPERATION::ADD_NEW)
    {
        ui->pushButton_add->show();
        ui->lineEdit_serial_number->show();
    }
    else {
       loadDatabase();
       ui->pushButton_remove->show();
       ui->pushButton_update->show();
       ui->comboBox_serial_number->show();
       connect(ui->comboBox_serial_number,SIGNAL(currentIndexChanged(int)),this,SLOT(loadItemSettings()));
    }
}

Service::~Service()
{
    delete ui;
}


void Service::clearForm()
{
    ui->lineEdit_serial_number->clear();
    ui->lineEdit_lot_time_hours->clear();
    ui->lineEdit_down_time_hours->clear();
    ui->lineEdit_compressor_call_hours->clear();
}

void Service::loadDatabase()
{
    ui->comboBox_serial_number->clear();
    QSqlQuery query(*_db);
    QList<int> id_list;
    query.exec("SELECT serial,lottimehours,downtimehours,compressorcallhours FROM services");
    while(query.next())
        id_list << query.value(0).toInt();
    foreach(int id ,id_list)
        ui->comboBox_serial_number->addItem(QString::number(id));
    if(!id_list.isEmpty())
        loadItemSettings();
}

void Service::loadItemSettings()
{
    if(ui->comboBox_serial_number->count() > 0)
    {
        QSqlQuery query(*_db);

        query.prepare("SELECT serial,lottimehours,downtimehours,compressorcallhours FROM services WHERE serial = :serial ");
        query.bindValue(":serial", ui->comboBox_serial_number->currentText().toInt());
        query.exec();

        while(query.next())
        {
            ui->lineEdit_lot_time_hours->setText(query.value(1).toString());
            ui->lineEdit_down_time_hours->setText(query.value(2).toString());
            ui->lineEdit_compressor_call_hours->setText(query.value(3).toString());
        }
        ui->label_status->setText("");
    }
}

void Service::on_pushButton_add_clicked()
{
    QSqlQuery query(*_db);

    query.prepare("INSERT INTO services (serial,lottimehours,downtimehours,compressorcallhours) Values   \
                  (:serial,:lottimehours,:downtimehours,:compressorcallhours)  ");

    query.bindValue(":serial"    , ui->lineEdit_serial_number->text().toInt());
    query.bindValue(":lottimehours" , ui->lineEdit_lot_time_hours->text().toDouble());
    query.bindValue(":downtimehours" ,ui->lineEdit_down_time_hours->text().toDouble());
    query.bindValue(":compressorcallhours",ui->lineEdit_compressor_call_hours->text().toDouble());

    if(query.exec()){
        ui->label_status->setText("Record Added ...");
        clearForm();
    }
    else
        ui->label_status->setText("Error in Record Add ...");

}

void Service::on_pushButton_close_clicked()
{
    delete this;
}

void Service::on_pushButton_update_clicked()
{
    QSqlQuery query(*_db);

    query.prepare("UPDATE services SET \
                  lottimehours = :lottimehours ,  downtimehours = :downtimehours ,\
                  compressorcallhours = :compressorcallhours \
                  WHERE serial = :serial  ");
    query.bindValue(":lottimehours" , ui->lineEdit_lot_time_hours->text().toDouble());
    query.bindValue(":downtimehours" ,ui->lineEdit_down_time_hours->text().toDouble());
    query.bindValue(":compressorcallhours"   ,ui->lineEdit_compressor_call_hours->text().toDouble());
    query.bindValue(":serial"    , ui->comboBox_serial_number->currentText().toInt());


    if(query.exec())
        ui->label_status->setText("Record Updated ...");
}

void Service::on_pushButton_remove_clicked()
{
    QSqlQuery query(*_db);
    query.prepare("DELETE FROM services WHERE serial = :serial ");
    query.bindValue(":serial", ui->comboBox_serial_number->currentText().toInt());
    if(query.exec()){
        ui->label_status->setText("Record Deleted ...");
        loadDatabase();
    }
}

