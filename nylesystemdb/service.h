#ifndef SERVICE_H
#define SERVICE_H

#include <QWidget>
#include <QtSql>

namespace Ui {
class Service;
}

class Service : public QWidget
{
    Q_OBJECT
public:
    enum OPERATION{
        ADD_NEW,
        UPDATE
    };

public:
    explicit Service(QSqlDatabase*,OPERATION,QWidget *parent = 0);
    ~Service();
public slots:
    void on_pushButton_add_clicked();
    void on_pushButton_close_clicked();
    void on_pushButton_update_clicked();
    void on_pushButton_remove_clicked();

    void loadDatabase();
    void loadItemSettings();
    void clearForm();

private:
    Ui::Service *ui;
    QSqlDatabase *_db;
};

#endif // SERVICE_H
