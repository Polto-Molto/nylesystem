#ifndef MODBUS_ACTION_H
#define MODBUS_ACTION_H

#include <QObject>
#include <QVariant>
#include <QModbusTcpClient>
#include <QDebug>

class modbus_action : public QObject
{
    Q_OBJECT
public:
    explicit modbus_action(int id,QObject *parent = 0);

signals:

public slots:
    bool connectToModbus(QString ip ,int port);
    void printData();

    // getter function
    int    CycleStoppedIndicator(){return _CycleStoppedIndicator;}
    int    DataLoggingIndicator(){return _DataLoggingIndicator;}
    qreal  ForwardDrybulb(){return _ForwardDrybulb;}
    qreal  ReverseDrybulb(){return _ReverseDrybulb;}
    qreal  Wetbulb(){return _Wetbulb;}
    qreal  WetbulbSetpoint(){return _WetbulbSetpoint;}
    qreal  DrybulbSetpoint(){return _DrybulbSetpoint;}
    int    KilnStart(){return _KilnStart;}
    qreal  KilnRuntime(){return _KilnRuntime;}
    int    HeatIndicator(){return _HeatIndicator;}
    int    ForwardFanIndicator(){return _ForwardFanIndicator;}
    int    ReverseFanIndicator(){return _ReverseFanIndicator;}
    int    BlowerIndicator(){return _BlowerIndicator;}
    int    SolenoidValveIndicator(){return _SolenoidValveIndicator;}
    int    CompressorIndicator(){return _CompressorIndicator;}
    int    SprayerIndicator(){return _SprayerIndicator;}
    int    ExhaustVentIndicator(){return _ExhaustVentIndicator;}
    int    SprayDumpIndicator(){return _SprayDumpIndicator;}
    int    StopButton(){return _StopButton;}
    int    StartButton(){return _StartButton;}
    int    Mode(){return _Mode;}

    // setter function
    bool  setForwardDrybulb(qreal);
    bool  setReverseDrybulb(qreal);
    bool  setWetbulb(qreal);
    bool  setWetbulbSetpoint(qreal);
    bool  setDrybulbSetpoint(qreal);
    bool  setKilnStart(int);
    bool  setStopButton(int);
    bool  setStartButton(int);
    bool  setMode(int);

private:
    int _id;
    QModbusTcpClient *_mbus;
    QVector<quint16> values;
    QModbusDataUnit data;
    QModbusReply * rply= NULL;

    int    _CycleStoppedIndicator;
    int    _DataLoggingIndicator;
    qreal  _ForwardDrybulb;
    qreal  _ReverseDrybulb;
    qreal  _Wetbulb;
    qreal  _WetbulbSetpoint;
    qreal  _DrybulbSetpoint;
    int    _KilnStart;
    qreal  _KilnRuntime;
    int    _HeatIndicator;
    int    _ForwardFanIndicator;
    int    _ReverseFanIndicator;
    int    _BlowerIndicator;
    int    _SolenoidValveIndicator;
    int    _CompressorIndicator;
    int    _SprayerIndicator;
    int    _ExhaustVentIndicator;
    int    _SprayDumpIndicator;
    int    _StopButton;
    int    _StartButton;
    int    _Mode;

};

#endif // MODBUS_ACTION_H
