#ifndef DATASETTINGS_H
#define DATASETTINGS_H

#include <QObject>
#include <QSettings>

#include "datatypes.h"
#include "datamanager.h"

class dataSettings : public QObject
{
    Q_OBJECT
public:
    explicit dataSettings(dataManager *,QObject *parent = 0);

signals:

public slots:
    void setSettingsFiles(QString,QString ,QString);

    void loadMainData();
    void updateMainData(MAIN_DATA);

    void loadKilnMachinesData();
    void saveKilnData(KILN);

    void loadLotsData();
    void saveLotData(LOT);

private:
    dataManager *_datamgr;
    QSettings *main_settings;
    QSettings *kilns_settings;
    QSettings *lots_settings;
};

#endif // DATASETTINGS_H
