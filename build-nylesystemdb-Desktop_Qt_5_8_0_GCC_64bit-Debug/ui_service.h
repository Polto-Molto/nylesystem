/********************************************************************************
** Form generated from reading UI file 'service.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SERVICE_H
#define UI_SERVICE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Service
{
public:
    QVBoxLayout *verticalLayout_3;
    QFrame *frame;
    QHBoxLayout *horizontalLayout;
    QLabel *label_6;
    QSpacerItem *horizontalSpacer_4;
    QFrame *frame_2;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_3;
    QVBoxLayout *verticalLayout;
    QLabel *label_5;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer;
    QVBoxLayout *verticalLayout_2;
    QComboBox *comboBox_serial_number;
    QLineEdit *lineEdit_serial_number;
    QLineEdit *lineEdit_lot_time_hours;
    QLineEdit *lineEdit_down_time_hours;
    QLineEdit *lineEdit_compressor_call_hours;
    QSpacerItem *horizontalSpacer_2;
    QFrame *frame_3;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label;
    QLabel *label_status;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *pushButton_add;
    QPushButton *pushButton_update;
    QPushButton *pushButton_remove;
    QPushButton *pushButton_close;
    QSpacerItem *horizontalSpacer_6;

    void setupUi(QWidget *Service)
    {
        if (Service->objectName().isEmpty())
            Service->setObjectName(QStringLiteral("Service"));
        Service->resize(620, 300);
        Service->setMinimumSize(QSize(620, 300));
        Service->setMaximumSize(QSize(620, 300));
        verticalLayout_3 = new QVBoxLayout(Service);
        verticalLayout_3->setSpacing(0);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(10, 0, 10, 0);
        frame = new QFrame(Service);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setMinimumSize(QSize(600, 50));
        frame->setMaximumSize(QSize(600, 50));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(9, 9, 9, -1);
        label_6 = new QLabel(frame);
        label_6->setObjectName(QStringLiteral("label_6"));
        QFont font;
        font.setPointSize(18);
        label_6->setFont(font);

        horizontalLayout->addWidget(label_6);

        horizontalSpacer_4 = new QSpacerItem(407, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);


        verticalLayout_3->addWidget(frame);

        frame_2 = new QFrame(Service);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setMinimumSize(QSize(600, 160));
        frame_2->setMaximumSize(QSize(600, 160));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        horizontalLayout_2 = new QHBoxLayout(frame_2);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer_3 = new QSpacerItem(30, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label_5 = new QLabel(frame_2);
        label_5->setObjectName(QStringLiteral("label_5"));
        QFont font1;
        font1.setPointSize(14);
        label_5->setFont(font1);

        verticalLayout->addWidget(label_5);

        label_3 = new QLabel(frame_2);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setFont(font1);

        verticalLayout->addWidget(label_3);

        label_4 = new QLabel(frame_2);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setFont(font1);

        verticalLayout->addWidget(label_4);

        label_2 = new QLabel(frame_2);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setFont(font1);

        verticalLayout->addWidget(label_2);


        horizontalLayout_2->addLayout(verticalLayout);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        comboBox_serial_number = new QComboBox(frame_2);
        comboBox_serial_number->setObjectName(QStringLiteral("comboBox_serial_number"));

        verticalLayout_2->addWidget(comboBox_serial_number);

        lineEdit_serial_number = new QLineEdit(frame_2);
        lineEdit_serial_number->setObjectName(QStringLiteral("lineEdit_serial_number"));

        verticalLayout_2->addWidget(lineEdit_serial_number);

        lineEdit_lot_time_hours = new QLineEdit(frame_2);
        lineEdit_lot_time_hours->setObjectName(QStringLiteral("lineEdit_lot_time_hours"));

        verticalLayout_2->addWidget(lineEdit_lot_time_hours);

        lineEdit_down_time_hours = new QLineEdit(frame_2);
        lineEdit_down_time_hours->setObjectName(QStringLiteral("lineEdit_down_time_hours"));

        verticalLayout_2->addWidget(lineEdit_down_time_hours);

        lineEdit_compressor_call_hours = new QLineEdit(frame_2);
        lineEdit_compressor_call_hours->setObjectName(QStringLiteral("lineEdit_compressor_call_hours"));

        verticalLayout_2->addWidget(lineEdit_compressor_call_hours);


        horizontalLayout_2->addLayout(verticalLayout_2);

        horizontalSpacer_2 = new QSpacerItem(130, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);


        verticalLayout_3->addWidget(frame_2);

        frame_3 = new QFrame(Service);
        frame_3->setObjectName(QStringLiteral("frame_3"));
        frame_3->setMinimumSize(QSize(600, 50));
        frame_3->setMaximumSize(QSize(600, 50));
        frame_3->setFrameShape(QFrame::NoFrame);
        frame_3->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_3);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label = new QLabel(frame_3);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout_3->addWidget(label);

        label_status = new QLabel(frame_3);
        label_status->setObjectName(QStringLiteral("label_status"));

        horizontalLayout_3->addWidget(label_status);

        horizontalSpacer_5 = new QSpacerItem(206, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_5);

        pushButton_add = new QPushButton(frame_3);
        pushButton_add->setObjectName(QStringLiteral("pushButton_add"));
        QFont font2;
        font2.setPointSize(12);
        pushButton_add->setFont(font2);

        horizontalLayout_3->addWidget(pushButton_add);

        pushButton_update = new QPushButton(frame_3);
        pushButton_update->setObjectName(QStringLiteral("pushButton_update"));
        pushButton_update->setFont(font2);

        horizontalLayout_3->addWidget(pushButton_update);

        pushButton_remove = new QPushButton(frame_3);
        pushButton_remove->setObjectName(QStringLiteral("pushButton_remove"));
        pushButton_remove->setFont(font2);

        horizontalLayout_3->addWidget(pushButton_remove);

        pushButton_close = new QPushButton(frame_3);
        pushButton_close->setObjectName(QStringLiteral("pushButton_close"));
        pushButton_close->setFont(font2);
        pushButton_close->setFlat(false);

        horizontalLayout_3->addWidget(pushButton_close);

        horizontalSpacer_6 = new QSpacerItem(20, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_6);


        verticalLayout_3->addWidget(frame_3);


        retranslateUi(Service);

        QMetaObject::connectSlotsByName(Service);
    } // setupUi

    void retranslateUi(QWidget *Service)
    {
        Service->setWindowTitle(QApplication::translate("Service", "Form", Q_NULLPTR));
        label_6->setText(QApplication::translate("Service", "Service Setting", Q_NULLPTR));
        label_5->setText(QApplication::translate("Service", "Serial Number:", Q_NULLPTR));
        label_3->setText(QApplication::translate("Service", "Lot Time Hours:", Q_NULLPTR));
        label_4->setText(QApplication::translate("Service", "Down Time Hours:", Q_NULLPTR));
        label_2->setText(QApplication::translate("Service", "Compressor Call Hours:", Q_NULLPTR));
        label->setText(QApplication::translate("Service", "status : ", Q_NULLPTR));
        label_status->setText(QString());
        pushButton_add->setText(QApplication::translate("Service", "Add", Q_NULLPTR));
        pushButton_update->setText(QApplication::translate("Service", "Update", Q_NULLPTR));
        pushButton_remove->setText(QApplication::translate("Service", "Remove", Q_NULLPTR));
        pushButton_close->setText(QApplication::translate("Service", "Close", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class Service: public Ui_Service {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SERVICE_H
