/********************************************************************************
** Form generated from reading UI file 'customer.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CUSTOMER_H
#define UI_CUSTOMER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_customer
{
public:
    QVBoxLayout *verticalLayout_2;
    QFrame *frame_2;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_8;
    QSpacerItem *horizontalSpacer_3;
    QFrame *frame_4;
    QVBoxLayout *verticalLayout;
    QFrame *frame;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_5;
    QLabel *label;
    QSpacerItem *horizontalSpacer_4;
    QLineEdit *lineEdit_id;
    QComboBox *comboBox_id;
    QSpacerItem *horizontalSpacer_6;
    QSpacerItem *verticalSpacer_2;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_9;
    QLabel *label_2;
    QLineEdit *lineEdit_first_name;
    QSpacerItem *horizontalSpacer_8;
    QLabel *label_3;
    QLineEdit *lineEdit_last_name;
    QSpacerItem *horizontalSpacer_7;
    QGroupBox *groupBox_2;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_10;
    QLabel *label_5;
    QSpacerItem *horizontalSpacer_17;
    QLineEdit *lineEdit_latitude;
    QSpacerItem *horizontalSpacer_12;
    QLabel *label_4;
    QLineEdit *lineEdit_longitude;
    QSpacerItem *horizontalSpacer_11;
    QSpacerItem *verticalSpacer_4;
    QGroupBox *groupBox_3;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *horizontalSpacer_13;
    QLabel *label_7;
    QSpacerItem *horizontalSpacer_16;
    QLineEdit *lineEdit_email;
    QSpacerItem *horizontalSpacer_14;
    QLabel *label_6;
    QSpacerItem *horizontalSpacer_18;
    QLineEdit *lineEdit_phone;
    QSpacerItem *horizontalSpacer_15;
    QSpacerItem *verticalSpacer_5;
    QFrame *frame_3;
    QHBoxLayout *horizontalLayout;
    QLabel *label_9;
    QLabel *label_status;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_add;
    QPushButton *pushButton_update;
    QPushButton *pushButton_remove;
    QPushButton *pushButton_close;
    QSpacerItem *horizontalSpacer_2;

    void setupUi(QWidget *customer)
    {
        if (customer->objectName().isEmpty())
            customer->setObjectName(QStringLiteral("customer"));
        customer->resize(620, 550);
        customer->setMinimumSize(QSize(620, 550));
        customer->setMaximumSize(QSize(620, 550));
        verticalLayout_2 = new QVBoxLayout(customer);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(10, -1, -1, -1);
        frame_2 = new QFrame(customer);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setMinimumSize(QSize(600, 60));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_2);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label_8 = new QLabel(frame_2);
        label_8->setObjectName(QStringLiteral("label_8"));
        QFont font;
        font.setPointSize(18);
        label_8->setFont(font);

        horizontalLayout_3->addWidget(label_8);

        horizontalSpacer_3 = new QSpacerItem(360, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);


        verticalLayout_2->addWidget(frame_2);

        frame_4 = new QFrame(customer);
        frame_4->setObjectName(QStringLiteral("frame_4"));
        frame_4->setMinimumSize(QSize(600, 450));
        frame_4->setMaximumSize(QSize(600, 450));
        frame_4->setFrameShape(QFrame::StyledPanel);
        frame_4->setFrameShadow(QFrame::Raised);
        verticalLayout = new QVBoxLayout(frame_4);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, -1, 0, -1);
        frame = new QFrame(frame_4);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setMinimumSize(QSize(600, 60));
        frame->setFrameShape(QFrame::NoFrame);
        frame->setFrameShadow(QFrame::Raised);
        frame->setLineWidth(1);
        horizontalLayout_4 = new QHBoxLayout(frame);
        horizontalLayout_4->setSpacing(0);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(0, -1, 0, -1);
        horizontalSpacer_5 = new QSpacerItem(73, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_5);

        label = new QLabel(frame);
        label->setObjectName(QStringLiteral("label"));
        label->setMinimumSize(QSize(26, 42));
        label->setMaximumSize(QSize(26, 42));
        QFont font1;
        font1.setPointSize(16);
        label->setFont(font1);

        horizontalLayout_4->addWidget(label);

        horizontalSpacer_4 = new QSpacerItem(30, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_4);

        lineEdit_id = new QLineEdit(frame);
        lineEdit_id->setObjectName(QStringLiteral("lineEdit_id"));
        lineEdit_id->setMinimumSize(QSize(150, 30));
        lineEdit_id->setMaximumSize(QSize(150, 30));

        horizontalLayout_4->addWidget(lineEdit_id);

        comboBox_id = new QComboBox(frame);
        comboBox_id->setObjectName(QStringLiteral("comboBox_id"));
        comboBox_id->setMinimumSize(QSize(150, 30));
        comboBox_id->setMaximumSize(QSize(150, 30));

        horizontalLayout_4->addWidget(comboBox_id);

        horizontalSpacer_6 = new QSpacerItem(251, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_6);


        verticalLayout->addWidget(frame);

        verticalSpacer_2 = new QSpacerItem(20, 14, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);

        groupBox = new QGroupBox(frame_4);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setMinimumSize(QSize(600, 0));
        groupBox->setMaximumSize(QSize(590, 16777215));
        QFont font2;
        font2.setPointSize(14);
        groupBox->setFont(font2);
        groupBox->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);
        horizontalLayout_2 = new QHBoxLayout(groupBox);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, -1, 0, -1);
        horizontalSpacer_9 = new QSpacerItem(21, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_9);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setMinimumSize(QSize(99, 30));
        label_2->setMaximumSize(QSize(99, 30));

        horizontalLayout_2->addWidget(label_2);

        lineEdit_first_name = new QLineEdit(groupBox);
        lineEdit_first_name->setObjectName(QStringLiteral("lineEdit_first_name"));
        lineEdit_first_name->setMinimumSize(QSize(150, 30));
        lineEdit_first_name->setMaximumSize(QSize(150, 30));

        horizontalLayout_2->addWidget(lineEdit_first_name);

        horizontalSpacer_8 = new QSpacerItem(18, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_8);

        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setMinimumSize(QSize(96, 30));
        label_3->setMaximumSize(QSize(96, 30));

        horizontalLayout_2->addWidget(label_3);

        lineEdit_last_name = new QLineEdit(groupBox);
        lineEdit_last_name->setObjectName(QStringLiteral("lineEdit_last_name"));
        lineEdit_last_name->setMinimumSize(QSize(150, 30));
        lineEdit_last_name->setMaximumSize(QSize(150, 30));

        horizontalLayout_2->addWidget(lineEdit_last_name);

        horizontalSpacer_7 = new QSpacerItem(20, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_7);


        verticalLayout->addWidget(groupBox);

        groupBox_2 = new QGroupBox(frame_4);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setMinimumSize(QSize(600, 0));
        groupBox_2->setMaximumSize(QSize(600, 16777215));
        groupBox_2->setFont(font2);
        groupBox_2->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);
        horizontalLayout_5 = new QHBoxLayout(groupBox_2);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(0, -1, 0, -1);
        horizontalSpacer_10 = new QSpacerItem(21, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_10);

        label_5 = new QLabel(groupBox_2);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setMinimumSize(QSize(78, 30));
        label_5->setMaximumSize(QSize(78, 30));

        horizontalLayout_5->addWidget(label_5);

        horizontalSpacer_17 = new QSpacerItem(15, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_17);

        lineEdit_latitude = new QLineEdit(groupBox_2);
        lineEdit_latitude->setObjectName(QStringLiteral("lineEdit_latitude"));
        lineEdit_latitude->setMinimumSize(QSize(150, 30));
        lineEdit_latitude->setMaximumSize(QSize(150, 30));

        horizontalLayout_5->addWidget(lineEdit_latitude);

        horizontalSpacer_12 = new QSpacerItem(17, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_12);

        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setMinimumSize(QSize(93, 30));
        label_4->setMaximumSize(QSize(93, 30));

        horizontalLayout_5->addWidget(label_4);

        lineEdit_longitude = new QLineEdit(groupBox_2);
        lineEdit_longitude->setObjectName(QStringLiteral("lineEdit_longitude"));
        lineEdit_longitude->setMinimumSize(QSize(150, 30));
        lineEdit_longitude->setMaximumSize(QSize(150, 30));

        horizontalLayout_5->addWidget(lineEdit_longitude);

        horizontalSpacer_11 = new QSpacerItem(19, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_11);


        verticalLayout->addWidget(groupBox_2);

        verticalSpacer_4 = new QSpacerItem(20, 13, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_4);

        groupBox_3 = new QGroupBox(frame_4);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setMinimumSize(QSize(600, 0));
        groupBox_3->setMaximumSize(QSize(600, 16777215));
        groupBox_3->setFont(font2);
        groupBox_3->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);
        groupBox_3->setFlat(false);
        horizontalLayout_6 = new QHBoxLayout(groupBox_3);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(0, -1, 0, -1);
        horizontalSpacer_13 = new QSpacerItem(22, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_13);

        label_7 = new QLabel(groupBox_3);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setMinimumSize(QSize(58, 30));
        label_7->setMaximumSize(QSize(58, 30));

        horizontalLayout_6->addWidget(label_7);

        horizontalSpacer_16 = new QSpacerItem(34, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_16);

        lineEdit_email = new QLineEdit(groupBox_3);
        lineEdit_email->setObjectName(QStringLiteral("lineEdit_email"));
        lineEdit_email->setMinimumSize(QSize(150, 30));
        lineEdit_email->setMaximumSize(QSize(150, 30));

        horizontalLayout_6->addWidget(lineEdit_email);

        horizontalSpacer_14 = new QSpacerItem(18, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_14);

        label_6 = new QLabel(groupBox_3);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setMinimumSize(QSize(60, 30));
        label_6->setMaximumSize(QSize(60, 30));

        horizontalLayout_6->addWidget(label_6);

        horizontalSpacer_18 = new QSpacerItem(32, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_18);

        lineEdit_phone = new QLineEdit(groupBox_3);
        lineEdit_phone->setObjectName(QStringLiteral("lineEdit_phone"));
        lineEdit_phone->setMinimumSize(QSize(150, 30));
        lineEdit_phone->setMaximumSize(QSize(150, 30));

        horizontalLayout_6->addWidget(lineEdit_phone);

        horizontalSpacer_15 = new QSpacerItem(22, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_15);


        verticalLayout->addWidget(groupBox_3);

        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_5);

        frame_3 = new QFrame(frame_4);
        frame_3->setObjectName(QStringLiteral("frame_3"));
        frame_3->setMinimumSize(QSize(600, 0));
        frame_3->setMaximumSize(QSize(600, 16777215));
        frame_3->setFrameShape(QFrame::NoFrame);
        frame_3->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_3);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, -1, 0, -1);
        label_9 = new QLabel(frame_3);
        label_9->setObjectName(QStringLiteral("label_9"));

        horizontalLayout->addWidget(label_9);

        label_status = new QLabel(frame_3);
        label_status->setObjectName(QStringLiteral("label_status"));

        horizontalLayout->addWidget(label_status);

        horizontalSpacer = new QSpacerItem(224, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_add = new QPushButton(frame_3);
        pushButton_add->setObjectName(QStringLiteral("pushButton_add"));
        QFont font3;
        font3.setPointSize(12);
        pushButton_add->setFont(font3);

        horizontalLayout->addWidget(pushButton_add);

        pushButton_update = new QPushButton(frame_3);
        pushButton_update->setObjectName(QStringLiteral("pushButton_update"));
        pushButton_update->setFont(font3);

        horizontalLayout->addWidget(pushButton_update);

        pushButton_remove = new QPushButton(frame_3);
        pushButton_remove->setObjectName(QStringLiteral("pushButton_remove"));
        pushButton_remove->setFont(font3);

        horizontalLayout->addWidget(pushButton_remove);

        pushButton_close = new QPushButton(frame_3);
        pushButton_close->setObjectName(QStringLiteral("pushButton_close"));
        pushButton_close->setFont(font3);
        pushButton_close->setFlat(false);

        horizontalLayout->addWidget(pushButton_close);

        horizontalSpacer_2 = new QSpacerItem(20, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout->addWidget(frame_3);


        verticalLayout_2->addWidget(frame_4);

        frame_4->raise();
        frame_2->raise();

        retranslateUi(customer);

        QMetaObject::connectSlotsByName(customer);
    } // setupUi

    void retranslateUi(QWidget *customer)
    {
        customer->setWindowTitle(QApplication::translate("customer", "Form", Q_NULLPTR));
        label_8->setText(QApplication::translate("customer", "Add New Customer", Q_NULLPTR));
        label->setText(QApplication::translate("customer", "ID:", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("customer", "Name", Q_NULLPTR));
        label_2->setText(QApplication::translate("customer", "First Name:", Q_NULLPTR));
        label_3->setText(QApplication::translate("customer", "Last Name:", Q_NULLPTR));
        groupBox_2->setTitle(QApplication::translate("customer", "Position", Q_NULLPTR));
        label_5->setText(QApplication::translate("customer", "Latitude:", Q_NULLPTR));
        label_4->setText(QApplication::translate("customer", "Longitude:", Q_NULLPTR));
        groupBox_3->setTitle(QApplication::translate("customer", "Contact", Q_NULLPTR));
        label_7->setText(QApplication::translate("customer", "E-mail:", Q_NULLPTR));
        label_6->setText(QApplication::translate("customer", "Phone:", Q_NULLPTR));
        label_9->setText(QApplication::translate("customer", "status : ", Q_NULLPTR));
        label_status->setText(QString());
        pushButton_add->setText(QApplication::translate("customer", "Add", Q_NULLPTR));
        pushButton_update->setText(QApplication::translate("customer", "Update", Q_NULLPTR));
        pushButton_remove->setText(QApplication::translate("customer", "Remove", Q_NULLPTR));
        pushButton_close->setText(QApplication::translate("customer", "Close", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class customer: public Ui_customer {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CUSTOMER_H
