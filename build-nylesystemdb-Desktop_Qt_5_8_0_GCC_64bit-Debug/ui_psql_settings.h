/********************************************************************************
** Form generated from reading UI file 'psql_settings.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PSQL_SETTINGS_H
#define UI_PSQL_SETTINGS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_psql_settings
{
public:
    QVBoxLayout *verticalLayout;
    QFormLayout *formLayout;
    QLabel *label;
    QLineEdit *lineEdit_host;
    QLabel *label_2;
    QLineEdit *lineEdit_user;
    QLabel *label_3;
    QLineEdit *lineEdit_pass;
    QLabel *label_4;
    QLineEdit *lineEdit_dbname;
    QLabel *label_6;
    QLabel *label_status;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton_cancel;
    QPushButton *pushButton_test_connection;
    QPushButton *pushButton_save;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *psql_settings)
    {
        if (psql_settings->objectName().isEmpty())
            psql_settings->setObjectName(QStringLiteral("psql_settings"));
        psql_settings->resize(400, 213);
        verticalLayout = new QVBoxLayout(psql_settings);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        formLayout = new QFormLayout();
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label = new QLabel(psql_settings);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        lineEdit_host = new QLineEdit(psql_settings);
        lineEdit_host->setObjectName(QStringLiteral("lineEdit_host"));

        formLayout->setWidget(0, QFormLayout::FieldRole, lineEdit_host);

        label_2 = new QLabel(psql_settings);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        lineEdit_user = new QLineEdit(psql_settings);
        lineEdit_user->setObjectName(QStringLiteral("lineEdit_user"));

        formLayout->setWidget(1, QFormLayout::FieldRole, lineEdit_user);

        label_3 = new QLabel(psql_settings);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_3);

        lineEdit_pass = new QLineEdit(psql_settings);
        lineEdit_pass->setObjectName(QStringLiteral("lineEdit_pass"));

        formLayout->setWidget(2, QFormLayout::FieldRole, lineEdit_pass);

        label_4 = new QLabel(psql_settings);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_4);

        lineEdit_dbname = new QLineEdit(psql_settings);
        lineEdit_dbname->setObjectName(QStringLiteral("lineEdit_dbname"));

        formLayout->setWidget(3, QFormLayout::FieldRole, lineEdit_dbname);

        label_6 = new QLabel(psql_settings);
        label_6->setObjectName(QStringLiteral("label_6"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_6);

        label_status = new QLabel(psql_settings);
        label_status->setObjectName(QStringLiteral("label_status"));
        label_status->setMinimumSize(QSize(0, 30));
        label_status->setMaximumSize(QSize(16777215, 30));
        label_status->setFrameShape(QFrame::Box);

        formLayout->setWidget(4, QFormLayout::FieldRole, label_status);


        verticalLayout->addLayout(formLayout);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        pushButton_cancel = new QPushButton(psql_settings);
        pushButton_cancel->setObjectName(QStringLiteral("pushButton_cancel"));

        horizontalLayout->addWidget(pushButton_cancel);

        pushButton_test_connection = new QPushButton(psql_settings);
        pushButton_test_connection->setObjectName(QStringLiteral("pushButton_test_connection"));

        horizontalLayout->addWidget(pushButton_test_connection);

        pushButton_save = new QPushButton(psql_settings);
        pushButton_save->setObjectName(QStringLiteral("pushButton_save"));

        horizontalLayout->addWidget(pushButton_save);


        verticalLayout->addLayout(horizontalLayout);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        retranslateUi(psql_settings);

        QMetaObject::connectSlotsByName(psql_settings);
    } // setupUi

    void retranslateUi(QWidget *psql_settings)
    {
        psql_settings->setWindowTitle(QApplication::translate("psql_settings", "Form", Q_NULLPTR));
        label->setText(QApplication::translate("psql_settings", "host", Q_NULLPTR));
        lineEdit_host->setText(QApplication::translate("psql_settings", "127.0.0.1", Q_NULLPTR));
        label_2->setText(QApplication::translate("psql_settings", "user", Q_NULLPTR));
        lineEdit_user->setText(QApplication::translate("psql_settings", "nylesystem", Q_NULLPTR));
        label_3->setText(QApplication::translate("psql_settings", "password", Q_NULLPTR));
        lineEdit_pass->setText(QApplication::translate("psql_settings", "nyle", Q_NULLPTR));
        label_4->setText(QApplication::translate("psql_settings", "database name", Q_NULLPTR));
        lineEdit_dbname->setText(QApplication::translate("psql_settings", "nyledb", Q_NULLPTR));
        label_6->setText(QApplication::translate("psql_settings", "status", Q_NULLPTR));
        label_status->setText(QString());
        pushButton_cancel->setText(QApplication::translate("psql_settings", "cancel", Q_NULLPTR));
        pushButton_test_connection->setText(QApplication::translate("psql_settings", "Test Connection", Q_NULLPTR));
        pushButton_save->setText(QApplication::translate("psql_settings", "save", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class psql_settings: public Ui_psql_settings {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PSQL_SETTINGS_H
