/********************************************************************************
** Form generated from reading UI file 'dbwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DBWINDOW_H
#define UI_DBWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_dbWindow
{
public:
    QAction *actionCustomer_new;
    QAction *actionCustomer_update;
    QAction *actionKiln_new;
    QAction *actionKiln_update;
    QAction *actionService_update;
    QAction *actionService_new;
    QAction *actionCharge_update;
    QAction *actionCharge_new;
    QAction *actionDatabase_Connection_setting;
    QAction *actionexit;
    QWidget *centralWidget;
    QMenuBar *menuBar;
    QMenu *menuNew;
    QMenu *menuUpdate_Remove;
    QMenu *menuSetting;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *dbWindow)
    {
        if (dbWindow->objectName().isEmpty())
            dbWindow->setObjectName(QStringLiteral("dbWindow"));
        dbWindow->resize(573, 411);
        actionCustomer_new = new QAction(dbWindow);
        actionCustomer_new->setObjectName(QStringLiteral("actionCustomer_new"));
        actionCustomer_new->setCheckable(false);
        actionCustomer_new->setChecked(false);
        actionCustomer_update = new QAction(dbWindow);
        actionCustomer_update->setObjectName(QStringLiteral("actionCustomer_update"));
        actionKiln_new = new QAction(dbWindow);
        actionKiln_new->setObjectName(QStringLiteral("actionKiln_new"));
        actionKiln_update = new QAction(dbWindow);
        actionKiln_update->setObjectName(QStringLiteral("actionKiln_update"));
        actionService_update = new QAction(dbWindow);
        actionService_update->setObjectName(QStringLiteral("actionService_update"));
        actionService_new = new QAction(dbWindow);
        actionService_new->setObjectName(QStringLiteral("actionService_new"));
        actionCharge_update = new QAction(dbWindow);
        actionCharge_update->setObjectName(QStringLiteral("actionCharge_update"));
        actionCharge_new = new QAction(dbWindow);
        actionCharge_new->setObjectName(QStringLiteral("actionCharge_new"));
        actionDatabase_Connection_setting = new QAction(dbWindow);
        actionDatabase_Connection_setting->setObjectName(QStringLiteral("actionDatabase_Connection_setting"));
        actionexit = new QAction(dbWindow);
        actionexit->setObjectName(QStringLiteral("actionexit"));
        centralWidget = new QWidget(dbWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        dbWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(dbWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 573, 22));
        menuNew = new QMenu(menuBar);
        menuNew->setObjectName(QStringLiteral("menuNew"));
        menuUpdate_Remove = new QMenu(menuBar);
        menuUpdate_Remove->setObjectName(QStringLiteral("menuUpdate_Remove"));
        menuSetting = new QMenu(menuBar);
        menuSetting->setObjectName(QStringLiteral("menuSetting"));
        dbWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(dbWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        dbWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(dbWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        dbWindow->setStatusBar(statusBar);

        menuBar->addAction(menuNew->menuAction());
        menuBar->addAction(menuUpdate_Remove->menuAction());
        menuBar->addAction(menuSetting->menuAction());
        menuNew->addAction(actionCustomer_new);
        menuNew->addAction(actionKiln_new);
        menuNew->addAction(actionService_new);
        menuNew->addAction(actionCharge_new);
        menuNew->addAction(actionexit);
        menuUpdate_Remove->addAction(actionCustomer_update);
        menuUpdate_Remove->addAction(actionKiln_update);
        menuUpdate_Remove->addAction(actionService_update);
        menuUpdate_Remove->addAction(actionCharge_update);
        menuSetting->addAction(actionDatabase_Connection_setting);

        retranslateUi(dbWindow);

        QMetaObject::connectSlotsByName(dbWindow);
    } // setupUi

    void retranslateUi(QMainWindow *dbWindow)
    {
        dbWindow->setWindowTitle(QApplication::translate("dbWindow", "dbWindow", Q_NULLPTR));
        actionCustomer_new->setText(QApplication::translate("dbWindow", "Customer", Q_NULLPTR));
        actionCustomer_update->setText(QApplication::translate("dbWindow", "Customer", Q_NULLPTR));
        actionKiln_new->setText(QApplication::translate("dbWindow", "Kiln", Q_NULLPTR));
        actionKiln_update->setText(QApplication::translate("dbWindow", "Kiln", Q_NULLPTR));
        actionService_update->setText(QApplication::translate("dbWindow", "Service", Q_NULLPTR));
        actionService_new->setText(QApplication::translate("dbWindow", "Service", Q_NULLPTR));
        actionCharge_update->setText(QApplication::translate("dbWindow", "Charge", Q_NULLPTR));
        actionCharge_new->setText(QApplication::translate("dbWindow", "Charge", Q_NULLPTR));
        actionDatabase_Connection_setting->setText(QApplication::translate("dbWindow", "Database Connection", Q_NULLPTR));
        actionexit->setText(QApplication::translate("dbWindow", "exit", Q_NULLPTR));
        menuNew->setTitle(QApplication::translate("dbWindow", "New", Q_NULLPTR));
        menuUpdate_Remove->setTitle(QApplication::translate("dbWindow", "Update \\ Remove", Q_NULLPTR));
        menuSetting->setTitle(QApplication::translate("dbWindow", "Setting", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class dbWindow: public Ui_dbWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DBWINDOW_H
