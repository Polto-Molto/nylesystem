/****************************************************************************
** Meta object code from reading C++ file 'dbwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../nylesystemdb/dbwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'dbwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_dbWindow_t {
    QByteArrayData data[10];
    char stringdata0[159];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_dbWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_dbWindow_t qt_meta_stringdata_dbWindow = {
    {
QT_MOC_LITERAL(0, 0, 8), // "dbWindow"
QT_MOC_LITERAL(1, 9, 9), // "connectdb"
QT_MOC_LITERAL(2, 19, 0), // ""
QT_MOC_LITERAL(3, 20, 20), // "open_addNew_customer"
QT_MOC_LITERAL(4, 41, 20), // "open_update_customer"
QT_MOC_LITERAL(5, 62, 16), // "open_addNew_kiln"
QT_MOC_LITERAL(6, 79, 16), // "open_update_kiln"
QT_MOC_LITERAL(7, 96, 19), // "open_addNew_service"
QT_MOC_LITERAL(8, 116, 19), // "open_update_service"
QT_MOC_LITERAL(9, 136, 22) // "open_psql_settings_dlg"

    },
    "dbWindow\0connectdb\0\0open_addNew_customer\0"
    "open_update_customer\0open_addNew_kiln\0"
    "open_update_kiln\0open_addNew_service\0"
    "open_update_service\0open_psql_settings_dlg"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_dbWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x0a /* Public */,
       3,    0,   55,    2, 0x0a /* Public */,
       4,    0,   56,    2, 0x0a /* Public */,
       5,    0,   57,    2, 0x0a /* Public */,
       6,    0,   58,    2, 0x0a /* Public */,
       7,    0,   59,    2, 0x0a /* Public */,
       8,    0,   60,    2, 0x0a /* Public */,
       9,    0,   61,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void dbWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        dbWindow *_t = static_cast<dbWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->connectdb(); break;
        case 1: _t->open_addNew_customer(); break;
        case 2: _t->open_update_customer(); break;
        case 3: _t->open_addNew_kiln(); break;
        case 4: _t->open_update_kiln(); break;
        case 5: _t->open_addNew_service(); break;
        case 6: _t->open_update_service(); break;
        case 7: _t->open_psql_settings_dlg(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject dbWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_dbWindow.data,
      qt_meta_data_dbWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *dbWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *dbWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_dbWindow.stringdata0))
        return static_cast<void*>(const_cast< dbWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int dbWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
