/********************************************************************************
** Form generated from reading UI file 'kiln.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_KILN_H
#define UI_KILN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_kiln
{
public:
    QVBoxLayout *verticalLayout_3;
    QFrame *frame;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label;
    QSpacerItem *horizontalSpacer_6;
    QFrame *frame_2;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_3;
    QVBoxLayout *verticalLayout;
    QLabel *label_3;
    QLabel *label_2;
    QLabel *label_4;
    QLabel *label_5;
    QSpacerItem *horizontalSpacer;
    QVBoxLayout *verticalLayout_2;
    QComboBox *comboBox_id;
    QLineEdit *lineEdit_kiln_id;
    QComboBox *comboBox_customer_id;
    QLineEdit *lineEdit_kiln_type;
    QLineEdit *lineEdit_serial_number;
    QSpacerItem *horizontalSpacer_2;
    QFrame *frame_3;
    QHBoxLayout *horizontalLayout;
    QLabel *label_6;
    QLabel *label_status;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *pushButton_add;
    QPushButton *pushButton_update;
    QPushButton *pushButton_remove;
    QPushButton *pushButton_close;
    QSpacerItem *horizontalSpacer_5;

    void setupUi(QWidget *kiln)
    {
        if (kiln->objectName().isEmpty())
            kiln->setObjectName(QStringLiteral("kiln"));
        kiln->resize(620, 300);
        kiln->setMinimumSize(QSize(620, 300));
        kiln->setMaximumSize(QSize(620, 300));
        verticalLayout_3 = new QVBoxLayout(kiln);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(10, -1, 10, -1);
        frame = new QFrame(kiln);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setMinimumSize(QSize(600, 50));
        frame->setMaximumSize(QSize(600, 50));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label = new QLabel(frame);
        label->setObjectName(QStringLiteral("label"));
        QFont font;
        font.setPointSize(18);
        label->setFont(font);

        horizontalLayout_3->addWidget(label);

        horizontalSpacer_6 = new QSpacerItem(425, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_6);


        verticalLayout_3->addWidget(frame);

        frame_2 = new QFrame(kiln);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setMinimumSize(QSize(600, 160));
        frame_2->setMaximumSize(QSize(600, 160));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        horizontalLayout_2 = new QHBoxLayout(frame_2);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer_3 = new QSpacerItem(30, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(10);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label_3 = new QLabel(frame_2);
        label_3->setObjectName(QStringLiteral("label_3"));
        QFont font1;
        font1.setPointSize(14);
        label_3->setFont(font1);

        verticalLayout->addWidget(label_3);

        label_2 = new QLabel(frame_2);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setFont(font1);

        verticalLayout->addWidget(label_2);

        label_4 = new QLabel(frame_2);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setFont(font1);

        verticalLayout->addWidget(label_4);

        label_5 = new QLabel(frame_2);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setFont(font1);

        verticalLayout->addWidget(label_5);


        horizontalLayout_2->addLayout(verticalLayout);

        horizontalSpacer = new QSpacerItem(20, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        comboBox_id = new QComboBox(frame_2);
        comboBox_id->setObjectName(QStringLiteral("comboBox_id"));

        verticalLayout_2->addWidget(comboBox_id);

        lineEdit_kiln_id = new QLineEdit(frame_2);
        lineEdit_kiln_id->setObjectName(QStringLiteral("lineEdit_kiln_id"));

        verticalLayout_2->addWidget(lineEdit_kiln_id);

        comboBox_customer_id = new QComboBox(frame_2);
        comboBox_customer_id->setObjectName(QStringLiteral("comboBox_customer_id"));

        verticalLayout_2->addWidget(comboBox_customer_id);

        lineEdit_kiln_type = new QLineEdit(frame_2);
        lineEdit_kiln_type->setObjectName(QStringLiteral("lineEdit_kiln_type"));

        verticalLayout_2->addWidget(lineEdit_kiln_type);

        lineEdit_serial_number = new QLineEdit(frame_2);
        lineEdit_serial_number->setObjectName(QStringLiteral("lineEdit_serial_number"));

        verticalLayout_2->addWidget(lineEdit_serial_number);


        horizontalLayout_2->addLayout(verticalLayout_2);

        horizontalSpacer_2 = new QSpacerItem(182, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);


        verticalLayout_3->addWidget(frame_2);

        frame_3 = new QFrame(kiln);
        frame_3->setObjectName(QStringLiteral("frame_3"));
        frame_3->setMinimumSize(QSize(600, 50));
        frame_3->setMaximumSize(QSize(600, 50));
        frame_3->setFrameShape(QFrame::NoFrame);
        frame_3->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_3);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label_6 = new QLabel(frame_3);
        label_6->setObjectName(QStringLiteral("label_6"));

        horizontalLayout->addWidget(label_6);

        label_status = new QLabel(frame_3);
        label_status->setObjectName(QStringLiteral("label_status"));

        horizontalLayout->addWidget(label_status);

        horizontalSpacer_4 = new QSpacerItem(206, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);

        pushButton_add = new QPushButton(frame_3);
        pushButton_add->setObjectName(QStringLiteral("pushButton_add"));
        QFont font2;
        font2.setPointSize(12);
        pushButton_add->setFont(font2);

        horizontalLayout->addWidget(pushButton_add);

        pushButton_update = new QPushButton(frame_3);
        pushButton_update->setObjectName(QStringLiteral("pushButton_update"));
        pushButton_update->setFont(font2);

        horizontalLayout->addWidget(pushButton_update);

        pushButton_remove = new QPushButton(frame_3);
        pushButton_remove->setObjectName(QStringLiteral("pushButton_remove"));
        pushButton_remove->setFont(font2);

        horizontalLayout->addWidget(pushButton_remove);

        pushButton_close = new QPushButton(frame_3);
        pushButton_close->setObjectName(QStringLiteral("pushButton_close"));
        pushButton_close->setFont(font2);
        pushButton_close->setFlat(false);

        horizontalLayout->addWidget(pushButton_close);

        horizontalSpacer_5 = new QSpacerItem(20, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_5);


        verticalLayout_3->addWidget(frame_3);


        retranslateUi(kiln);

        QMetaObject::connectSlotsByName(kiln);
    } // setupUi

    void retranslateUi(QWidget *kiln)
    {
        kiln->setWindowTitle(QApplication::translate("kiln", "Form", Q_NULLPTR));
        label->setText(QApplication::translate("kiln", "Add New Kiln", Q_NULLPTR));
        label_3->setText(QApplication::translate("kiln", "Kiln ID:", Q_NULLPTR));
        label_2->setText(QApplication::translate("kiln", "Customer ID:", Q_NULLPTR));
        label_4->setText(QApplication::translate("kiln", "Kiln Type:", Q_NULLPTR));
        label_5->setText(QApplication::translate("kiln", "Serial Number:", Q_NULLPTR));
        label_6->setText(QApplication::translate("kiln", "status : ", Q_NULLPTR));
        label_status->setText(QString());
        pushButton_add->setText(QApplication::translate("kiln", "Add", Q_NULLPTR));
        pushButton_update->setText(QApplication::translate("kiln", "Update", Q_NULLPTR));
        pushButton_remove->setText(QApplication::translate("kiln", "Remove", Q_NULLPTR));
        pushButton_close->setText(QApplication::translate("kiln", "Close", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class kiln: public Ui_kiln {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_KILN_H
