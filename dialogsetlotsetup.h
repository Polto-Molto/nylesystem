#ifndef DIALOGSETLOTSETUP_H
#define DIALOGSETLOTSETUP_H

#include <QDialog>
#include "datamanager.h"
#include "dialogschedule.h"
#include <modbus_action.h>

namespace Ui {
class DialogSetLotSetup;
}

class DialogSetLotSetup : public QDialog
{
    Q_OBJECT

public:
    explicit DialogSetLotSetup(dataManager*,int,modbus_action*,QWidget *parent = 0);
    ~DialogSetLotSetup();

public slots:
    void setLotValues();
    void setLotParameters();

    void on_pushButton_cancel_clicked();
    void on_pushButton_apply_clicked();
    void on_pushButton_ok_clicked();
    void on_pushButton_schedule_clicked();

private:
    Ui::DialogSetLotSetup *ui;
    dataManager *_datamgr;
    modbus_action* _mbus;
    int lot_id;
    int kiln_id;
};

#endif // DIALOGSETLOTSETUP_H
