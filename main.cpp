#include <QApplication>
#include <datamanager.h>
#include <datasettings.h>
#include "mainscreen.h"
#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    dataManager* dataMgr = new dataManager;

    dataSettings* dataSet = new dataSettings(dataMgr);
    dataSet->setSettingsFiles("main.ini","kiln.ini","lots.ini");
    dataSet->loadMainData();
    dataSet->loadKilnMachinesData();
    dataSet->loadLotsData();

    mainscreen sm(dataMgr);
    sm.show();
    return a.exec();
}
