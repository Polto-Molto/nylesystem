#ifndef DIALOGNEWLOTSELECTKILN_H
#define DIALOGNEWLOTSELECTKILN_H

#include <QDialog>
#include "datamanager.h"
#include "kilnselectionitem.h"

namespace Ui {
class DialogNewLotSelectKiln;
}

class DialogNewLotSelectKiln : public QDialog
{
    Q_OBJECT

public:
    explicit DialogNewLotSelectKiln(dataManager*,QWidget *parent = 0);
    ~DialogNewLotSelectKiln();

signals:
    void selectedKiln(KILN);

public slots:
    int getNewLotID();
    void getFreeKiln();

    void on_pushButton_cancel_clicked();
    void on_pushButton_create_clicked();
    void on_pushButton_cancel_kiln_clicked();
    void on_pushButton_finish_clicked();
    void setCurrentKiln(KILN);
    int currentKilnID();

private:
    Ui::DialogNewLotSelectKiln *ui;
    dataManager* _datamgr;
    QVector<kilnSelectionItem*> free_kilns;
    KILN current_kiln;
};

#endif // DIALOGNEWLOTSELECTKILN_H
