#ifndef SCREEN_5_H
#define SCREEN_5_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_5;
}

class screen_5 : public QWidget
{
    Q_OBJECT

public:
    explicit screen_5(dataManager*,QWidget *parent = 0);
    ~screen_5();

private:
    Ui::screen_5 *ui;
    dataManager* _datamgr;
};

#endif // SCREEN_5_H
