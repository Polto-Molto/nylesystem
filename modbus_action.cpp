#include "modbus_action.h"

modbus_action::modbus_action(int id,QObject *parent) :
    QObject(parent),
    _id(id)
{
    _mbus = 0;
}

bool modbus_action::connectToModbus(QString ip ,int port)
{
  _mbus = new QModbusTcpClient();
  _mbus->setConnectionParameter(QModbusDevice::NetworkAddressParameter,QVariant(ip));
  _mbus->setConnectionParameter(QModbusDevice::NetworkPortParameter,QVariant(port));
  return _mbus->connectDevice();
}

/*
 * setter functions
 * 4
6
8
10
12
15

 */

bool  modbus_action::setForwardDrybulb(qreal v)
{
    values.clear();
    values.push_back(v);
    data = QModbusDataUnit(QModbusDataUnit::HoldingRegisters, 4 , values );
    rply = _mbus->sendWriteRequest(data ,_id);
    connect(rply,SIGNAL(finished()) , this ,SLOT(printData()));
}

bool  modbus_action::setReverseDrybulb(qreal v)
{
    values.clear();
    values.push_back(v);
    data = QModbusDataUnit(QModbusDataUnit::HoldingRegisters, 6 , values );
    rply = _mbus->sendWriteRequest(data ,_id);
    connect(rply,SIGNAL(finished()) , this ,SLOT(printData()));
}

bool  modbus_action::setWetbulb(qreal v)
{
    values.clear();
    values.push_back(v);
    data = QModbusDataUnit(QModbusDataUnit::HoldingRegisters, 8 , values );
    rply = _mbus->sendWriteRequest(data ,_id);
    connect(rply,SIGNAL(finished()) , this ,SLOT(printData()));
}

bool  modbus_action::setWetbulbSetpoint(qreal v)
{
    values.clear();
    values.push_back(v);
    data = QModbusDataUnit(QModbusDataUnit::HoldingRegisters, 10 , values );
    rply = _mbus->sendWriteRequest(data ,_id);
    connect(rply,SIGNAL(finished()) , this ,SLOT(printData()));
}

bool  modbus_action::setDrybulbSetpoint(qreal v)
{
    values.clear();
    values.push_back(v);
    data = QModbusDataUnit(QModbusDataUnit::HoldingRegisters, 12 , values );
    rply = _mbus->sendWriteRequest(data ,_id);
    connect(rply,SIGNAL(finished()) , this ,SLOT(printData()));
}

bool  modbus_action::setKilnStart(int v)
{
    values.clear();
    values.push_back(v);
    data = QModbusDataUnit(QModbusDataUnit::HoldingRegisters, 15 , values );
    rply = _mbus->sendWriteRequest(data ,_id);
    connect(rply,SIGNAL(finished()) , this ,SLOT(printData()));
}

bool  modbus_action::setStopButton(int v)
{
    values.clear();
    values.push_back(v);
    data = QModbusDataUnit(QModbusDataUnit::HoldingRegisters, 28 , values );
    rply = _mbus->sendWriteRequest(data ,_id);
    connect(rply,SIGNAL(finished()) , this ,SLOT(printData()));
}

bool  modbus_action::setStartButton(int v)
{
    values.clear();
    values.push_back(v);
    data = QModbusDataUnit(QModbusDataUnit::HoldingRegisters, 29 , values );
    rply = _mbus->sendWriteRequest(data ,_id);
    connect(rply,SIGNAL(finished()) , this ,SLOT(printData()));
}

bool  modbus_action::setMode(int v)
{
    values.clear();
    values.push_back(v);
    data = QModbusDataUnit(QModbusDataUnit::HoldingRegisters, 29 , values );
    rply = _mbus->sendWriteRequest(data ,_id);
    connect(rply,SIGNAL(finished()) , this ,SLOT(printData()));
}


void modbus_action::printData()
{
    int i =0;
    QModbusDataUnit::RegisterType type = rply->result().registerType();
    switch(type)
    {
    case QModbusDataUnit::Coils:
        break;

    case QModbusDataUnit::DiscreteInputs:
        break;

    case QModbusDataUnit::InputRegisters:
        break;

    case QModbusDataUnit::HoldingRegisters:
        qDebug() << rply->result().startAddress();
        break;
    }
}
