#include "datasettings.h"
#include <QDebug>

dataSettings::dataSettings(dataManager *dm,QObject *parent) :
    QObject(parent),
    _datamgr(dm)
{
    kilns_settings = 0;
    lots_settings = 0;
    main_settings = 0;
    connect(_datamgr,SIGNAL(mainDataUpdated(MAIN_DATA)),this,SLOT(updateMainData(MAIN_DATA)));

    connect(_datamgr,SIGNAL(kilnMachineUpdated(KILN)),this,SLOT(saveKilnData(KILN)));
    connect(_datamgr,SIGNAL(kilnMachineAdded(KILN)),this,SLOT(saveKilnData(KILN)));

    connect(_datamgr,SIGNAL(lotUpdated(LOT)),this,SLOT(saveLotData(LOT)));
    connect(_datamgr,SIGNAL(lotAdded(LOT)),this,SLOT(saveLotData(LOT)));
}

void dataSettings::setSettingsFiles(QString main_file,QString kiln_file, QString lot_file)
{
    main_settings = new QSettings(main_file,QSettings::IniFormat);
    kilns_settings = new QSettings(kiln_file,QSettings::IniFormat);
    lots_settings = new QSettings(lot_file,QSettings::IniFormat);
}

void dataSettings::loadMainData()
{
    MAIN_DATA main_data;
    main_settings->beginGroup("main");
    main_data.kiln_count = main_settings->value("kiln_count").toInt();
    main_data.lot_count = main_settings->value("lot_count").toInt();
    main_data.total_lots_count = main_settings->value("total_lots_count").toInt();
    main_settings->endGroup();
    _datamgr->setMainData(main_data);
}

void dataSettings::updateMainData(MAIN_DATA main_data)
{
    main_settings->beginGroup("main");
    main_settings->setValue("kiln_count",main_data.kiln_count);
    main_settings->setValue("lot_count",main_data.lot_count);
    main_settings->setValue("total_lots_count",main_data.total_lots_count);
    main_settings->endGroup();
}

void dataSettings::loadKilnMachinesData()
{
    QMap<int,KILN> Kilns_table;
    for(int i = 0 ; i < _datamgr->mainData()->kiln_count ;i++)
    {
        KILN kiln;
        kilns_settings->beginGroup("KILN_" + QString::number(i+1));
        kiln.id = kilns_settings->value("id").toInt();
        kiln.modbus_id = kilns_settings->value("modbus_id").toInt();
        kiln.lot_id = kilns_settings->value("lot_id").toInt();

        kiln.equipment.operate_status = (OPERATE_STATUS) kilns_settings->value("operate_status").toInt();

        kiln.equipment.fan_direction.opreration = (OPERATION) kilns_settings->value("fan_direction_opreration").toInt();
        kiln.equipment.fan_direction.direction = (DIRECTION) kilns_settings->value("fan_direction").toInt();
        kiln.equipment.fan_direction.forward_hours =  kilns_settings->value("fan_direction_forward_hours").toInt();
        kiln.equipment.fan_direction.reverse_hours = kilns_settings->value("fan_direction_reverse_hours").toInt();

        kiln.equipment.fan_speed.operation = (OPERATION) kilns_settings->value("fan_speed_operation").toInt();
        kiln.equipment.fan_speed.value = kilns_settings->value("fan_speed").toInt();

        kiln.equipment.vents.operation = (OPERATION) kilns_settings->value("vents_operation").toInt();
        kiln.equipment.vents.value = kilns_settings->value("vents").toInt();

        kiln.equipment.spray.operation = (OPERATION) kilns_settings->value("spray_operation").toInt();
        kiln.equipment.spray.value = kilns_settings->value("spray").toInt();

        kiln.equipment.drive_info.current = kilns_settings->value("drive_info_current").toInt();
        kiln.equipment.drive_info.hz = kilns_settings->value("drive_info_hz").toInt();
        kiln.equipment.drive_info.rpm = kilns_settings->value("drive_info_rpm").toInt();

        kiln.equipment.compressor.operation = (OPERATION) kilns_settings->value("compressor_operation").toInt();
        kiln.equipment.compressor.call = kilns_settings->value("compressor_call").toInt();
        kiln.equipment.compressor.ctd = kilns_settings->value("compressor_ctd").toInt();
        kiln.equipment.compressor.discharge = kilns_settings->value("compressor_discharge").toInt();
        kiln.equipment.compressor.dlp = kilns_settings->value("compressor_dlp").toInt();
        kiln.equipment.compressor.dlt = kilns_settings->value("compressor_dlt").toInt();
        kiln.equipment.compressor.oil = kilns_settings->value("compressor_oil").toInt();
        kiln.equipment.compressor.slp = kilns_settings->value("compressor_slp").toInt();
        kiln.equipment.compressor.slt = kilns_settings->value("compressor_slt").toInt();
        kiln.equipment.compressor.suction = kilns_settings->value("compressor_suction").toInt();
        kilns_settings->endGroup();
        if(kiln.id == 0)
            kiln.id =i +1;
        Kilns_table.insert(kiln.id , kiln);
    }
    _datamgr->setKilnTable(Kilns_table);

}

void dataSettings::saveKilnData(KILN kiln)
{
    kilns_settings->beginGroup("KILN_" + QString::number(kiln.id));
    kilns_settings->setValue("id",kiln.id);
    kilns_settings->setValue("modbus_id",kiln.modbus_id);
    kilns_settings->setValue("lot_id" , kiln.lot_id);
    kilns_settings->setValue("operate_status",(int)kiln.equipment.operate_status);
    kilns_settings->setValue("fan_direction_opreration",(int)kiln.equipment.fan_direction.opreration);
    kilns_settings->setValue("fan_direction",kiln.equipment.fan_direction.direction);
    kilns_settings->setValue("fan_direction_forward_hours",kiln.equipment.fan_direction.forward_hours);
    kilns_settings->setValue("fan_direction_reverse_hours",kiln.equipment.fan_direction.reverse_hours);
    kilns_settings->setValue("fan_speed_operation",(int)kiln.equipment.fan_speed.operation);
    kilns_settings->setValue("fan_speed",kiln.equipment.fan_speed.value);
    kilns_settings->setValue("vents_operation",(int)kiln.equipment.vents.operation);
    kilns_settings->setValue("vents",kiln.equipment.vents.value);
    kilns_settings->setValue("spray_operation",(int)kiln.equipment.spray.operation);
    kilns_settings->setValue("spray",kiln.equipment.spray.value);
    kilns_settings->setValue("drive_info_current",kiln.equipment.drive_info.current);
    kilns_settings->setValue("drive_info_hz",kiln.equipment.drive_info.hz);
    kilns_settings->setValue("drive_info_rpm",kiln.equipment.drive_info.rpm);
    kilns_settings->setValue("compressor_operation",(int)kiln.equipment.compressor.operation);
    kilns_settings->setValue("compressor_call",kiln.equipment.compressor.call);
    kilns_settings->setValue("compressor_ctd",kiln.equipment.compressor.ctd);
    kilns_settings->setValue("compressor_discharge",kiln.equipment.compressor.discharge);
    kilns_settings->setValue("compressor_dlp",kiln.equipment.compressor.dlp);
    kilns_settings->setValue("compressor_dlt",kiln.equipment.compressor.dlt);
    kilns_settings->setValue("compressor_oil",kiln.equipment.compressor.oil);
    kilns_settings->setValue("compressor_slp",kiln.equipment.compressor.slp);
    kilns_settings->setValue("compressor_slt",kiln.equipment.compressor.slt);
    kilns_settings->setValue("compressor_suction",kiln.equipment.compressor.suction);
    kilns_settings->endGroup();
}

void dataSettings::loadLotsData()
{
    QMap<int,LOT> lots_table;
    for(int i =0 ; i < _datamgr->mainData()->lot_count ;i++)
    {
        LOT lot;
        lots_settings->beginGroup("LOT_" + QString::number(i+1));

        lot.id = lots_settings->value("id").toInt();
        lot.lot_mode = (LOT_MODE) lots_settings->value("lot_mode").toInt();
        for(int j = 0 ; j<8 ; j++)
        {
            lots_settings->beginGroup("SCHEDULE_" + QString::number(j+1));
            lot.lot_schedule[j].db = lots_settings->value("db").toInt();
            lot.lot_schedule[j].dep = lots_settings->value("dep").toInt();
            lot.lot_schedule[j].emc = lots_settings->value("emc").toInt();
            lot.lot_schedule[j].fan_speed = lots_settings->value("fan_speed").toInt();
            lot.lot_schedule[j].left_hours = lots_settings->value("left_hours").toInt();
            lot.lot_schedule[j].pass_hours = lots_settings->value("pass_hours").toInt();
            lot.lot_schedule[j].wb = lots_settings->value("wb").toInt();
            lots_settings->endGroup();
        }
        lots_settings->endGroup();
        lots_table.insert(lot.id , lot);
    }
    _datamgr->setLotsTable(lots_table);
}

void dataSettings::saveLotData(LOT lot)
{
    lots_settings->beginGroup("LOT_" + QString::number(lot.id));
    lots_settings->setValue("id" , lot.id);
    lots_settings->setValue("lot_mode" , lot.lot_mode);
    for(int j = 0 ; j<8 ; j++)
    {
        lots_settings->beginGroup("SCHEDULE_" + QString::number(j+1));
        lots_settings->setValue("db",lot.lot_schedule[j].db);
        lots_settings->setValue("dep",lot.lot_schedule[j].dep);
        lots_settings->setValue("emc",lot.lot_schedule[j].emc);
        lots_settings->setValue("fan_speed",lot.lot_schedule[j].fan_speed);
        lots_settings->setValue("left_hours",lot.lot_schedule[j].left_hours);
        lots_settings->setValue("pass_hours",lot.lot_schedule[j].pass_hours);
        lots_settings->setValue("wb",lot.lot_schedule[j].wb);
        lots_settings->endGroup();
    }
    lots_settings->endGroup();
}
