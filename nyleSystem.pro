#-------------------------------------------------
#
# Project created by QtCreator 2017-04-19T04:32:35
#
#-------------------------------------------------

QT       += core gui serialbus

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = nyleSystem
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        widget.cpp \
    screen_1.cpp \
    screen_2.cpp \
    screen_3.cpp \
    screen_4.cpp \
    screen_5.cpp \
    datasettings.cpp \
    datamanager.cpp \
    kilnselectionitem.cpp \
    dialognewlotselectkiln.cpp \
    dialogsetkilnequipment.cpp \
    mainscreen.cpp \
    dialogsetlotsetup.cpp \
    dialogschedule.cpp \
    modbus_action.cpp

HEADERS  += widget.h \
    screen_1.h \
    screen_2.h \
    screen_3.h \
    screen_4.h \
    screen_5.h \
    datatypes.h \
    datasettings.h \
    datamanager.h \
    kilnselectionitem.h \
    dialognewlotselectkiln.h \
    dialogsetkilnequipment.h \
    mainscreen.h \
    dialogsetlotsetup.h \
    dialogschedule.h \
    modbus_action.h

FORMS    += widget.ui \
    screen_1.ui \
    screen_2.ui \
    screen_3.ui \
    screen_4.ui \
    screen_5.ui \
    kilnselectionitem.ui \
    dialognewlotselectkiln.ui \
    dialogsetkilnequipment.ui \
    mainscreen.ui \
    dialogsetlotsetup.ui \
    dialogschedule.ui

RESOURCES += \
    images.qrc
