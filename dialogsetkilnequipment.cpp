#include "dialogsetkilnequipment.h"
#include "ui_dialogsetkilnequipment.h"

DialogSetKilnEquipment::DialogSetKilnEquipment(dataManager *d,int id,modbus_action* m,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogSetKilnEquipment),
    _datamgr(d),
    kiln_id(id),
    _mbus(m)
{
    ui->setupUi(this);
    QButtonGroup* compressor_group = new QButtonGroup(this);
    compressor_group->addButton(ui->pushButton_compressor_auto);
    compressor_group->addButton(ui->pushButton_compressor_off);

    QButtonGroup* fan_speed_group = new QButtonGroup(this);
    fan_speed_group->addButton(ui->pushButton_fan_speed_auto);
    fan_speed_group->addButton(ui->pushButton_fan_speed_manual);
    connect(fan_speed_group,SIGNAL(buttonClicked(QAbstractButton*))
            ,this,SLOT(checkPushButtonClicked(QAbstractButton*)));

    QButtonGroup* fan_direction_operation_group = new QButtonGroup(this);
    fan_direction_operation_group->addButton(ui->pushButton_fan_direction_auto);
    fan_direction_operation_group->addButton(ui->pushButton_fan_direction_manual);
    fan_direction_operation_group->addButton(ui->pushButton_fan_direction_off);

    QButtonGroup* vents_group = new QButtonGroup(this);
    vents_group->addButton(ui->pushButton_vents_auto);
    vents_group->addButton(ui->pushButton_vents_manual);
    vents_group->addButton(ui->pushButton_vents_off);
    connect(vents_group,SIGNAL(buttonClicked(QAbstractButton*))
            ,this,SLOT(checkPushButtonClicked(QAbstractButton*)));

    QButtonGroup* spray_group = new QButtonGroup(this);
    spray_group->addButton(ui->pushButton_spray_off);
    spray_group->addButton(ui->pushButton_spray_auto);
    spray_group->addButton(ui->pushButton_spray_manual);
    connect(spray_group,SIGNAL(buttonClicked(QAbstractButton*))
            ,this,SLOT(checkPushButtonClicked(QAbstractButton*)));

    QButtonGroup* fan_direction_group = new QButtonGroup(this);
    fan_direction_group->addButton(ui->pushButton_fan_direction_fwd);
    fan_direction_group->addButton(ui->pushButton_fan_direction_rev);

    setKilnValues();
}

DialogSetKilnEquipment::~DialogSetKilnEquipment()
{
    delete ui;
}
void DialogSetKilnEquipment::checkPushButtonClicked(QAbstractButton* p)
{
    if(p == ui->pushButton_fan_speed_manual)
        ui->lineEdit_fan_speed->show();
    else if(p == ui->pushButton_fan_speed_auto)
        ui->lineEdit_fan_speed->hide();
    else if(p == ui->pushButton_spray_auto)
        ui->lineEdit_spray_value->hide();
    else if(p == ui->pushButton_spray_manual)
        ui->lineEdit_spray_value->show();
    else if(p == ui->pushButton_spray_off)
        ui->lineEdit_spray_value->hide();
    else if(p == ui->pushButton_vents_auto)
        ui->lineEdit_vents_value->hide();
    else if(p == ui->pushButton_vents_manual)
        ui->lineEdit_vents_value->show();
    else if(p == ui->pushButton_vents_off)
        ui->lineEdit_vents_value->hide();
}

void DialogSetKilnEquipment::setKilnValues()
{
    KILN kiln = _datamgr->kilnMachines()->value(kiln_id);
    switch(kiln.equipment.fan_direction.direction)
    {
    case DIRECTION::REVERSE :
        ui->pushButton_fan_direction_rev->setChecked(true);
        break;
    case DIRECTION::FORWARD :
        ui->pushButton_fan_direction_fwd->setChecked(true);
        break;
    }

    switch(kiln.equipment.fan_direction.opreration)
    {
    case OPERATION::AUTO :
        ui->pushButton_fan_direction_auto->setChecked(true);
        ui->lineEdit_fan_direction_fwd_hours->setText(QString::number(kiln.equipment.fan_direction.forward_hours));

        ui->lineEdit_fan_direction_rev_hours->setText(QString::number(kiln.equipment.fan_direction.reverse_hours));
        break;
    case OPERATION::MANUAL:
        ui->pushButton_fan_direction_manual->setChecked(true);
        break;
    case OPERATION::OFF:
        ui->pushButton_fan_direction_off->setChecked(true);
        break;
    }

    switch(kiln.equipment.fan_speed.operation)
    {
    case OPERATION::AUTO :
        ui->pushButton_fan_speed_auto->setChecked(true);
        ui->lineEdit_fan_speed->hide();
        break;
    case OPERATION::MANUAL:
        ui->pushButton_fan_speed_manual->setChecked(true);
        ui->lineEdit_fan_speed->show();
        ui->lineEdit_fan_speed->setText(QString::number(kiln.equipment.fan_speed.value));
        break;
    }

    switch(kiln.equipment.spray.operation)
    {
    case OPERATION::AUTO :
        ui->pushButton_spray_auto->setChecked(true);
        ui->lineEdit_spray_value->hide();
        break;
    case OPERATION::MANUAL:
        ui->pushButton_spray_manual->setChecked(true);
        ui->lineEdit_spray_value->show();
        ui->lineEdit_spray_value->setText(QString::number(kiln.equipment.spray.value));
        break;
    case OPERATION::OFF:
        ui->pushButton_spray_off->setChecked(true);
        ui->lineEdit_spray_value->hide();
        break;
    }

    switch(kiln.equipment.vents.operation)
    {
    case OPERATION::AUTO :
        ui->pushButton_vents_auto->setChecked(true);
        ui->lineEdit_vents_value->hide();
        break;
    case OPERATION::MANUAL:
        ui->pushButton_vents_manual->setChecked(true);
        ui->lineEdit_vents_value->show();
        ui->lineEdit_vents_value->setText(QString::number(kiln.equipment.vents.value));
        break;
    case OPERATION::OFF:
        ui->pushButton_vents_off->setChecked(true);
        ui->lineEdit_vents_value->hide();
        break;
    }

    switch(kiln.equipment.compressor.operation)
    {
    case OPERATION::AUTO :
        ui->pushButton_compressor_auto->setChecked(true);
        break;
    case OPERATION::OFF:
        ui->pushButton_compressor_off->setChecked(true);
        break;
    }

    ui->lineEdit_compressor_call->setText(QString::number(kiln.equipment.compressor.call));
    ui->lineEdit_compressor_ctd->setText(QString::number(kiln.equipment.compressor.ctd));
    ui->lineEdit_compressor_dlp->setText(QString::number(kiln.equipment.compressor.dlp));
    ui->lineEdit_compressor_dlt->setText(QString::number(kiln.equipment.compressor.dlt));
    ui->lineEdit_compressor_oil->setText(QString::number(kiln.equipment.compressor.oil));
    ui->lineEdit_compressor_slp->setText(QString::number(kiln.equipment.compressor.slp));
    ui->lineEdit_compressor_slt->setText(QString::number(kiln.equipment.compressor.slt));

    ui->lineEdit_drive_info_current->setText(QString::number(kiln.equipment.drive_info.current));
    ui->lineEdit_drive_info_hz->setText(QString::number(kiln.equipment.drive_info.hz));
    ui->lineEdit_drive_info_rpm->setText(QString::number(kiln.equipment.drive_info.rpm));

    ui->dial_compressor_suction->setValue(kiln.equipment.compressor.suction);
    ui->dial_compressor_discharge->setValue(kiln.equipment.compressor.discharge);
}

void DialogSetKilnEquipment::setEquipmentParameters()
{
    KILN kiln = _datamgr->kilnMachines()->value(kiln_id);
    if(ui->pushButton_fan_direction_fwd->isChecked())
        kiln.equipment.fan_direction.direction = DIRECTION::FORWARD;
    else
        kiln.equipment.fan_direction.direction = DIRECTION::REVERSE;

    if(ui->pushButton_fan_direction_auto->isChecked())
        kiln.equipment.fan_direction.opreration = OPERATION::AUTO;
    else if(ui->pushButton_fan_direction_manual->isChecked())
        kiln.equipment.fan_direction.opreration = OPERATION::MANUAL;
    else
        kiln.equipment.fan_direction.opreration = OPERATION::OFF;

    kiln.equipment.fan_direction.forward_hours = ui->lineEdit_fan_direction_fwd_hours->text().toInt();
    kiln.equipment.fan_direction.reverse_hours = ui->lineEdit_fan_direction_rev_hours->text().toInt();


    if(ui->pushButton_fan_speed_auto->isChecked())
        kiln.equipment.fan_speed.operation = OPERATION::AUTO;
    else
        kiln.equipment.fan_speed.operation = OPERATION::MANUAL;

    kiln.equipment.fan_speed.value = ui->lineEdit_fan_speed->text().toInt();

    if(ui->pushButton_vents_auto->isChecked())
        kiln.equipment.vents.operation = OPERATION::AUTO;
    else if(ui->pushButton_vents_manual->isChecked())
        kiln.equipment.vents.operation = OPERATION::MANUAL;
    else
        kiln.equipment.vents.operation = OPERATION::OFF;

    kiln.equipment.vents.value = ui->lineEdit_vents_value->text().toInt();

    if(ui->pushButton_spray_auto->isChecked())
        kiln.equipment.spray.operation = OPERATION::AUTO;
    else if(ui->pushButton_spray_manual->isChecked())
        kiln.equipment.spray.operation = OPERATION::MANUAL;
    else
        kiln.equipment.spray.operation = OPERATION::OFF;

    kiln.equipment.spray.value = ui->lineEdit_spray_value->text().toInt();

    kiln.equipment.drive_info.current = ui->lineEdit_drive_info_current->text().toInt();
    kiln.equipment.drive_info.rpm = ui->lineEdit_drive_info_rpm->text().toInt();
    kiln.equipment.drive_info.hz = ui->lineEdit_drive_info_hz->text().toInt();

    if(ui->pushButton_compressor_auto->isChecked())
        kiln.equipment.compressor.operation = OPERATION::AUTO;
    else
        kiln.equipment.compressor.operation = OPERATION::OFF;

    kiln.equipment.compressor.call = ui->lineEdit_compressor_call->text().toInt();
    kiln.equipment.compressor.ctd = ui->lineEdit_compressor_ctd->text().toInt();
    kiln.equipment.compressor.discharge = ui->dial_compressor_discharge->value();
    kiln.equipment.compressor.dlp = ui->lineEdit_compressor_dlp->text().toInt();
    kiln.equipment.compressor.dlt = ui->lineEdit_compressor_dlt->text().toInt();
    kiln.equipment.compressor.oil = ui->lineEdit_compressor_oil->text().toInt();
    kiln.equipment.compressor.slp = ui->lineEdit_compressor_slp->text().toInt();
    kiln.equipment.compressor.slt = ui->lineEdit_compressor_slt->text().toInt();
    kiln.equipment.compressor.suction = ui->dial_compressor_suction->value();

    _datamgr->updateKilnMachine(kiln);
}

void DialogSetKilnEquipment::on_pushButton_home_clicked()
{
    this->close();
}

void DialogSetKilnEquipment::on_pushButton_apply_clicked()
{
    this->setEquipmentParameters();
}

void DialogSetKilnEquipment::on_pushButton_ok_clicked()
{
    this->setEquipmentParameters();
    this->close();
}
