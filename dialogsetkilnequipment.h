#ifndef DIALOGSETKILNEQUIPMENT_H
#define DIALOGSETKILNEQUIPMENT_H

#include <QDialog>
#include "datamanager.h"
#include <QAbstractButton>
#include <modbus_action.h>

namespace Ui {
class DialogSetKilnEquipment;
}

class DialogSetKilnEquipment : public QDialog
{
    Q_OBJECT

public:
    explicit DialogSetKilnEquipment(dataManager*,int,modbus_action*,QWidget *parent = 0);
    ~DialogSetKilnEquipment();

public slots:
    void checkPushButtonClicked(QAbstractButton*);
    void setKilnValues();
    void setEquipmentParameters();

    void on_pushButton_home_clicked();
    void on_pushButton_apply_clicked();
    void on_pushButton_ok_clicked();

private:
    Ui::DialogSetKilnEquipment *ui;
    modbus_action* _mbus;
    dataManager* _datamgr;
    int lot_id;
    int kiln_id;
};

#endif // DIALOGSETKILNEQUIPMENT_H
